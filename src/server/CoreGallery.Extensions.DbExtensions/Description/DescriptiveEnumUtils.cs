﻿using System;
using System.Linq;
using System.Reflection;

namespace CoreGallery.Extensions.DbExtensions.Description
{
    public static class DescriptiveEnumUtils
    {
        public static string GetDescription(this Enum value)
        {
            return GetEnumDescription(value);
        }

        public static int GetOrder(this Enum value)
        {
            return GetEnumOrder(value);
        }

        private static string GetEnumDescription(object value)
        {
            if (value == null)
                return null;

            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            var descriptionAttributes = fieldInfo.GetCustomAttributes<DescriptionAttribute>(false).ToArray();
            if (!descriptionAttributes.Any())
            {
                var enforcementStyle = GetEnforcementStyleForClass(type);
                switch (enforcementStyle)
                {
                    case DescriptiveEnumEnforcementStyle.ThrowException:
                        throw new InvalidOperationException($"Value {value} of enum {type.Name} does not declare a description using the {typeof(DescriptionAttribute)}. " +
                                                            $"Either annotate enum value with {typeof(DescriptionAttribute)} or change the Enums {typeof(DescriptiveEnumEnforcementStyle)} to {DescriptiveEnumEnforcementStyle.DefaultToValue}.");
                    case DescriptiveEnumEnforcementStyle.DefaultToValue:
                        return GetEnumName(value);
                    case DescriptiveEnumEnforcementStyle.DefaultToEmptyString:
                        return string.Empty;
                    default:
                        throw new InvalidOperationException($"Invalid enforcement style {enforcementStyle}");
                }
            }

            if (descriptionAttributes.Length > 1)
                throw new InvalidOperationException($"Too many {typeof(DescriptionAttribute)}s defined for {type.Name}.{value}");

            return descriptionAttributes.First().Description;
        }

        public const int DEFAULT_ORDER = 99999;

        private static int GetEnumOrder(object value)
        {
            if (value == null)
                return DEFAULT_ORDER;

            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            var descriptionAttributes = fieldInfo.GetCustomAttributes<DescriptionAttribute>(false).ToArray();
            if (!descriptionAttributes.Any())
            {
                var enforcementStyle = GetEnforcementStyleForClass(type);
                switch (enforcementStyle)
                {
                    case DescriptiveEnumEnforcementStyle.ThrowException:
                        throw new InvalidOperationException($"Value {value} of enum {type.Name} does not declare a description using the {typeof(DescriptionAttribute)}. " +
                                                            $"Either annotate enum value with {typeof(DescriptionAttribute)} or change the Enums {typeof(DescriptiveEnumEnforcementStyle)} to {DescriptiveEnumEnforcementStyle.DefaultToValue}.");
                    case DescriptiveEnumEnforcementStyle.DefaultToValue:
                    case DescriptiveEnumEnforcementStyle.DefaultToEmptyString:
                        return DEFAULT_ORDER;
                    default:
                        throw new InvalidOperationException($"Invalid enforcement style {enforcementStyle}");
                }
            }

            if (descriptionAttributes.Length > 1)
                throw new InvalidOperationException($"Too many {typeof(DescriptionAttribute)}s defined for {type.Name}.{value}");

            return descriptionAttributes.First().Order;
        }

        private static string GetEnumName(object value)
        {
            return value.ToString();
        }

        private static DescriptiveEnumEnforcementStyle GetEnforcementStyleForClass(Type type)
        {
            var enforcementAttributes = type.GetCustomAttributes<DescriptiveEnumEnforcementAttribute>(false).ToArray();
            if (enforcementAttributes.Any())
                return enforcementAttributes.First().EnforcementStyle;
            throw new InvalidOperationException($"Enum {type.FullName} does not declare an enforcement style. Use the {typeof(DescriptiveEnumEnforcementAttribute)} to annotate your enum.");
        }
    }
}