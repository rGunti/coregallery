﻿namespace CoreGallery.Extensions.DbExtensions.Description
{
    public enum DescriptiveEnumEnforcementStyle
    {
        ThrowException,
        DefaultToValue,
        DefaultToEmptyString
    }
}