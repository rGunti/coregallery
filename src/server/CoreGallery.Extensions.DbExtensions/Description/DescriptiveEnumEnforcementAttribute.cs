﻿using System;

namespace CoreGallery.Extensions.DbExtensions.Description
{
    [AttributeUsage(AttributeTargets.Enum)]
    public class DescriptiveEnumEnforcementAttribute : Attribute
    {
        public DescriptiveEnumEnforcementAttribute()
            : this(DescriptiveEnumEnforcementStyle.DefaultToValue)
        { }

        public DescriptiveEnumEnforcementAttribute(DescriptiveEnumEnforcementStyle enforcementStyle)
        {
            EnforcementStyle = enforcementStyle;
        }

        public DescriptiveEnumEnforcementStyle EnforcementStyle { get; set; }
    }
}