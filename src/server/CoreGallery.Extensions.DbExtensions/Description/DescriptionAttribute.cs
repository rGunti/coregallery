﻿using System;

namespace CoreGallery.Extensions.DbExtensions.Description
{
    [AttributeUsage(AttributeTargets.Field)]
    public class DescriptionAttribute : Attribute
    {
        public DescriptionAttribute() { }
        public DescriptionAttribute(string description)
        {
            Description = description;
        }

        public string Description { get; set; }
        public int Order { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
