﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.DTOs
{
    public class UserDTO : ISimpleDTO<string>, ITrackedDTO, INonDestructibleDTO
    {
        public string ID { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public RoleDTO Role { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }

    public class TokenEnrichedUserDTO : UserDTO
    {
        public string Token { get; set; }
    }
}
