﻿using System;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.DTOs
{
    public class AlbumDTO : ISimpleDTO<int>, ITrackedDTO
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string UserID { get; set; }

        public bool IsPrivateAlbum { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
