﻿using System.Collections.Generic;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.DTOs
{
    public class RoleDTO : ISimpleDTO<RoleKey>
    {
        public RoleKey ID { get; set; }
        public string Description { get; set; }

        public IList<PermissionKey> Permissions { get; set; }
    }
}
