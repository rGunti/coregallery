﻿using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.DTOs
{
    public class PermissionDTO : ISimpleDTO<PermissionKey>
    {
        public PermissionKey ID { get; set; }
        public string Description { get; set; }
    }
}
