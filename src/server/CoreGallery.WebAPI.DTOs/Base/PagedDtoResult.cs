﻿using System.Collections.Generic;
using CoreGallery.Extensions.Paging;

namespace CoreGallery.WebAPI.DTOs.Base
{
    public interface IPagedDtoResult<TDto> :
        IPagedResult<TDto>
        where TDto : IDTO
    {
    }

    public class PagedDtoResult<TDto> :  PagedResult<TDto>, IPagedDtoResult<TDto>
        where TDto : IDTO
    {
        public PagedDtoResult()
        { }

        public PagedDtoResult(IEnumerable<TDto> items, int totalItems)
            : base(items, totalItems)
        { }
    }
}