﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreGallery.WebAPI.DTOs.Base
{
    public interface IDTO
    {
    }

    public interface ISimpleDTO<TPrimaryKey> : IDTO
    {
        TPrimaryKey ID { get; set; }
    }

    public interface ITrackedDTO
    {
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }

    public interface INonDestructibleDTO
    {
        DateTime? DeletedAt { get; set; }
    }
}
