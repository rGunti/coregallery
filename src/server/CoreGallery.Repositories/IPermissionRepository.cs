﻿using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Repositories
{
    public interface IPermissionRepository : ICrudEntityRepository<Permission, PermissionKey>
    { }
}