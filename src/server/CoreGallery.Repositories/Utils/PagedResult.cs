﻿using System.Collections.Generic;
using System.Linq;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Extensions.Paging;

namespace CoreGallery.Db.Repositories.Utils
{
    public interface IPagedQueryResult<TEntity> :
        IPagedResult<TEntity>
        where TEntity : IEntity
    { }

    public class PagedQueryResult<TEntity> : 
        PagedResult<TEntity>, IPagedQueryResult<TEntity>
        where TEntity : IEntity
    {
        public PagedQueryResult() : 
            base()
        { }
        public PagedQueryResult(IEnumerable<TEntity> items, int totalItems) : 
            base(items, totalItems)
        { }


    }

    public static class PagedQueryResultExtensions
    {
        public static IPagedQueryResult<TEntity> ToPagedResult<TEntity>(
            this IQueryable<TEntity> query,
            int page, int pageSize)
            where TEntity : IEntity
        {
            return new PagedQueryResult<TEntity>(
                query.Skip(page * pageSize).Take(pageSize).ToList(),
                query.Count());
        }
    }
}