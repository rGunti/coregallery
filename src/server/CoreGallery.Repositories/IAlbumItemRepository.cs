﻿using System.Collections.Generic;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.Db.Repositories.Utils;
using CoreGallery.Extensions.Paging;

namespace CoreGallery.Db.Repositories
{
    public interface IAlbumItemRepository : ICrudEntityRepository<AlbumItem, int>
    {
        IEnumerable<AlbumItem> GetItemsInAlbum(int albumId);
        IEnumerable<AlbumItem> GetItemsInAlbum(Album album);

        AlbumItem GetItemInAlbum(int albumId, int itemId);

        IPagedQueryResult<AlbumItem> GetPagedItemsInAlbum(int albumId, int page, int pageSize);

        INeighborhood<AlbumItem> GetNeighborhoodOfItem(int albumId, int itemId);
    }
}