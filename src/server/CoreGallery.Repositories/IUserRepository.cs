﻿using System.Collections.Generic;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Repositories
{
    public interface IUserRepository : ICrudEntityRepository<User, string>
    {
        IEnumerable<User> GetUsersWithRole(RoleKey role);
        User GetUserWithRoleAndPermissions(string username);
        IEnumerable<User> GetUsersWithRoleAndPermissions();
    }
}
