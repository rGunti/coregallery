﻿using System.Collections.Generic;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Base;

namespace CoreGallery.Db.Repositories
{
    public interface IAlbumRepository : ICrudEntityRepository<Album, int>
    {
        IEnumerable<Album> GetAlbumsOfUser(string username, bool includingPrivateAlbums = false);
        IEnumerable<Album> GetAlbumsForUser(string username);
    }
}