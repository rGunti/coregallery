﻿using System.Collections.Generic;
using CoreGallery.Db.Entities.Base;

namespace CoreGallery.Db.Repositories.Base
{
    public interface IRepository
    { }

    public interface IRepository<TEntity> : IRepository
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(object key);
    }

    public interface ICrudRepository<TEntity> : IRepository<TEntity>
    {
        TEntity Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }

    public interface IRepository<TEntity, TKey> : IRepository<TEntity>
    {
        TEntity Get(TKey key);
    }

    public interface ICrudRepository<TEntity, TKey> : ICrudRepository<TEntity>, IRepository<TEntity, TKey>
    {
        void Delete(TKey key);
    }

    public interface IEntityRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : ISimpleEntity<TKey>
    { }

    public interface ICrudEntityRepository<TEntity, TKey> : ICrudRepository<TEntity, TKey>
        where TEntity : ISimpleEntity<TKey>
    { }
}