﻿using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Repositories.Utils;

namespace CoreGallery.Db.Repositories.Base
{
    public interface IPagedRepository : IRepository
    { }

    public interface IPagedRepository<TEntity> :
        IPagedRepository,
        IRepository<TEntity>
        where TEntity : IEntity
    {
        IPagedQueryResult<TEntity> GetAll(int page, int pageSize);
    }
}