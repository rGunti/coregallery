﻿using System.Collections.Generic;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Repositories
{
    public interface IRoleRepository : ICrudEntityRepository<Role, RoleKey>
    {
        IEnumerable<PermissionKey> GetPermissionsInRole(RoleKey role);
    }
}