﻿using System;

namespace CoreGallery.Db.Repositories.Exceptions
{
    public class BaseDbException : ApplicationException
    {
        public BaseDbException()
        {
        }

        public BaseDbException(string message) : base(message)
        {
        }

        public BaseDbException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public class RecordNotFoundException : BaseDbException
    {
        public Type EntityType { get; }
        public object SearchParameter { get; }

        public RecordNotFoundException(Type entityType, object searchParameter)
            : base($"Record {entityType.Name} with value {searchParameter} could not be found.") { }
    }
}