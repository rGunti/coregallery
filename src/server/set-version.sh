#!/bin/bash
_ver=$(date +"%y%m%d%H%M")
_year=$(date +"%Y")
cat Directory.Build.gitlab.props | sed "s/\[TIMESTAMP\]/${_ver}/g" | sed "s/\[YEAR\]/${_year}/g" > Directory.Build.props
