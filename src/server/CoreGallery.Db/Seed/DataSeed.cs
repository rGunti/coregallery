﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Entities.Relations;
using CoreGallery.Extensions.DbExtensions.Description;
using CoreGallery.Extensions.Permissions;
using CoreGallery.Extensions.Permissions.PermissionAssignment;

namespace CoreGallery.Db.Seed
{
    public static class DataSeed
    {
        public static readonly Permission[] PERMISSION_SEED;
        public static readonly Role[] ROLE_SEED;
        public static readonly RolePermissionAssignment[] ROLE_PERM_ASSIGN_SEED;

        static DataSeed()
        {
            PERMISSION_SEED = GetAllPermissions();
            ROLE_SEED = GetAllRoles();
            ROLE_PERM_ASSIGN_SEED = GetAllRolePermAssignments();
        }

        private static Permission GetPermission(this PermissionKey permission)
            => new Permission() { ID = permission, Description = permission.GetDescription() };

        private static Permission[] GetAllPermissions()
        {
            return Enum.GetValues(typeof(PermissionKey)).Cast<PermissionKey>()
                .Select(p => p.GetPermission())
                .ToArray();
        }

        private static Role GetRole(this RoleKey role)
            => new Role() { ID = role, Description = role.GetDescription(), Order = role.GetOrder() };

        private static Role[] GetAllRoles()
        {
            return Enum.GetValues(typeof(RoleKey)).Cast<RoleKey>()
                .Select(r => r.GetRole())
                .ToArray();
        }

        private static RolePermissionAssignment[] GetAllRolePermAssignments()
        {
            var list = new List<RolePermissionAssignment>();
            foreach (RoleKey roleKey in Enum.GetValues(typeof(RoleKey)))
            {
                list.AddRange(
                    roleKey.GetAssignedPermissions()
                        .Select(p => new RolePermissionAssignment(roleKey, p))
                );
            }
            return list.ToArray();
        }
    }
}
