﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories.Utils;
using CoreGallery.Extensions.Paging;
using Microsoft.EntityFrameworkCore.Internal;

namespace CoreGallery.Db.Repositories.Impl
{
    public class AlbumItemRepository : BaseCrudRepository<AlbumItem, int>, IAlbumItemRepository
    {
        public AlbumItemRepository(CoreGalleryContext dbContext) : base(dbContext)
        {
        }

        protected override void UpdateDbEntity(AlbumItem entity, AlbumItem changedEntity)
        {
            entity.ItemName = changedEntity.ItemName;

            entity.ImageFormat = changedEntity.ImageFormat;
            entity.ImageWidth = changedEntity.ImageWidth;
            entity.ImageHeight = changedEntity.ImageHeight;
            entity.BitDepth = changedEntity.BitDepth;

            entity.Author = changedEntity.Author;
            entity.CopyrightNotice = changedEntity.CopyrightNotice;

            entity.TakenAt = changedEntity.TakenAt;

            entity.CameraMaker = changedEntity.CameraMaker;
            entity.CameraModel = changedEntity.CameraModel;

            entity.PositionDescription = changedEntity.PositionDescription;
            entity.PositionLatitude = changedEntity.PositionLatitude;
            entity.PositionLongitude = changedEntity.PositionLongitude;
            entity.PositionAltitude = changedEntity.PositionAltitude;
        }

        public IEnumerable<AlbumItem> GetItemsInAlbum(int albumId)
        {
            /* https://stackoverflow.com/questions/2814742/how-to-order-by-column-with-null-values-last-in-entity-framework */
            return dbContext.AlbumItems
                .Where(i => i.AlbumID == albumId)
                .OrderBy(i => i.TakenAt == null)  // "false" comes before "true", therefore this works
                .ThenBy(i => i.TakenAt)
                .ThenBy(i => i.CreatedAt);
        }

        public IEnumerable<AlbumItem> GetItemsInAlbum(Album album) => GetItemsInAlbum(album.ID);

        public AlbumItem GetItemInAlbum(int albumId, int itemId)
        {
            return dbContext.AlbumItems
                .FirstOrDefault(i => i.AlbumID == albumId && i.ID == itemId);
        }

        public IPagedQueryResult<AlbumItem> GetPagedItemsInAlbum(int albumId, int page, int pageSize)
        {
            /* https://stackoverflow.com/questions/2814742/how-to-order-by-column-with-null-values-last-in-entity-framework */
            return dbContext.AlbumItems
                .Where(i => i.AlbumID == albumId)
                .OrderBy(i => i.TakenAt == null)  // "false" comes before "true", therefore this works
                .ThenBy(i => i.TakenAt)
                .ThenBy(i => i.CreatedAt)
                .ToPagedResult(page, pageSize);
        }

        public INeighborhood<AlbumItem> GetNeighborhoodOfItem(int albumId, int itemId)
        {
            var current = GetItemInAlbum(albumId, itemId);
            if (current == null)
            {
                return new Neighborhood<AlbumItem>();
            }

            var previous = GetItemsInAlbum(albumId)
                .LastOrDefault(i => i.SortDate < current.SortDate);
            var next = GetItemsInAlbum(albumId)
                .FirstOrDefault(i => i.SortDate > current.SortDate);

            return new Neighborhood<AlbumItem>(current, previous, next);
        }
    }
}
