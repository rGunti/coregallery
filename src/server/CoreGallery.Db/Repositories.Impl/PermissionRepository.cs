﻿using CoreGallery.Db.Entities;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Repositories.Impl
{
    public class PermissionRepository : BaseCrudRepository<Permission, PermissionKey>, IPermissionRepository
    {
        public PermissionRepository(CoreGalleryContext dbContext) : base(dbContext)
        {
        }
        
        protected override void UpdateDbEntity(Permission dbEntity, Permission changedEntity)
        {
            dbEntity.Description = changedEntity.Description;
        }
    }
}