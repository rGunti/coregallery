﻿using System.Collections.Generic;
using System.Linq;
using CoreGallery.Db.Entities;

namespace CoreGallery.Db.Repositories.Impl
{
    public class AlbumRepository : BaseCrudRepository<Album, int>, IAlbumRepository
    {
        public AlbumRepository(CoreGalleryContext dbContext) : base(dbContext)
        {
        }

        protected override void UpdateDbEntity(Album dbEntity, Album changedEntity)
        {
            dbEntity.Name = changedEntity.Name;
            dbEntity.IsPrivateAlbum = changedEntity.IsPrivateAlbum;

            // Only change if given
            if (changedEntity.UserID != null)
                dbEntity.UserID = changedEntity.UserID;
        }

        public IEnumerable<Album> GetAlbumsOfUser(string username, bool includingPrivateAlbums = false)
        {
            var query = dbContext.Albums.Where(a => a.UserID == username);
            if (!includingPrivateAlbums)
                query = query.Where(a => !a.IsPrivateAlbum);

            return query;
        }

        public IEnumerable<Album> GetAlbumsForUser(string username)
        {
            return dbContext.Albums
                .Where(a => a.UserID == username || !a.IsPrivateAlbum);
        }
    }
}