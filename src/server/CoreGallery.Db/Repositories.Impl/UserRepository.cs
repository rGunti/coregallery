﻿using System.Collections.Generic;
using System.Linq;
using CoreGallery.Db.Entities;
using CoreGallery.Extensions.Permissions;
using Microsoft.EntityFrameworkCore;

namespace CoreGallery.Db.Repositories.Impl
{
    public class UserRepository : BaseCrudRepository<User, string>, IUserRepository
    {
        public UserRepository(CoreGalleryContext dbContext)
            : base(dbContext)
        { }

        public override User Create(User user)
        {
            user.Role = dbContext.Roles.Find(user.Role.ID);
            return base.Create(user);
        }

        protected override void UpdateDbEntity(User dbEntity, User changedEntity)
        {
            dbEntity.Email = changedEntity.Email;
            if (changedEntity.PasswordHash != null && changedEntity.PasswordSalt != null)
            {
                dbEntity.PasswordHash = changedEntity.PasswordHash;
                dbEntity.PasswordSalt = changedEntity.PasswordSalt;
            }
            dbEntity.Role = dbContext.Roles.Find(changedEntity.Role.ID);
        }

        public IEnumerable<User> GetUsersWithRole(RoleKey role)
        {
            return dbContext.Users
                .Include(u => u.Role)
                .Where(u => u.Role.ID == role);
        }

        public User GetUserWithRoleAndPermissions(string username)
        {
            return GetUsersWithRoleAndPermissions()
                .FirstOrDefault(u => u.ID == username);
        }

        public IEnumerable<User> GetUsersWithRoleAndPermissions()
        {
            return dbContext.Users
                .Include(u => u.Role)
                .Include(u => u.Role.RolePermissionAssignments)
                .OrderBy(u => u.ID);
        }
    }
}