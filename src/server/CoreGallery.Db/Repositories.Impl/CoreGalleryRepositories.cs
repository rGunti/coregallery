﻿using Microsoft.Extensions.DependencyInjection;

namespace CoreGallery.Db.Repositories.Impl
{
    public static class CoreGalleryRepositories
    {
        public static IServiceCollection AddCoreGalleryRepositories(this IServiceCollection services)
        {
            return services
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IRoleRepository, RoleRepository>()
                .AddScoped<IPermissionRepository, PermissionRepository>()
                .AddScoped<IAlbumRepository, AlbumRepository>()
                .AddScoped<IAlbumItemRepository, AlbumItemRepository>();
        }
    }
}
