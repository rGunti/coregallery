﻿using System.Collections.Generic;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.Db.Repositories.Exceptions;
using Microsoft.EntityFrameworkCore.Storage;

namespace CoreGallery.Db.Repositories.Impl
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly CoreGalleryContext dbContext;

        protected BaseRepository(CoreGalleryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual IEnumerable<TEntity> GetAll() => dbContext.Set<TEntity>();

        public virtual TEntity Get(object key) => dbContext.Set<TEntity>().Find(key);
    }

    public abstract class BaseCrudRepository<TEntity, TKey> : BaseRepository<TEntity>, ICrudEntityRepository<TEntity, TKey>
        where TEntity : class, ISimpleEntity<TKey>
    {
        protected BaseCrudRepository(CoreGalleryContext dbContext)
            : base(dbContext)
        {  }

        protected IDbContextTransaction BeginNewTransaction()
            => dbContext.Database.BeginTransaction();

        public virtual TEntity Get(TKey key) => base.Get(key);

        public virtual TEntity Create(TEntity entity)
        {
            using (var transaction = BeginNewTransaction())
            {
                var newEntity = dbContext.Add(entity);
                dbContext.SaveChanges();
                transaction.Commit();

                return newEntity.Entity;
            }
        }

        public virtual void Update(TEntity entity)
        {
            using (var transaction = BeginNewTransaction())
            {
                var existingEntity = Get(entity.ID);
                if (existingEntity == null)
                    throw new RecordNotFoundException(typeof(TEntity), entity.ID);
                UpdateDbEntity(existingEntity, entity);
                dbContext.SaveChanges();
                transaction.Commit();
            }
        }

        protected abstract void UpdateDbEntity(TEntity dbEntity, TEntity changedEntity);

        public virtual void Delete(TEntity entity) => Delete(entity.ID);

        public virtual void Delete(TKey key)
        {
            using (var transaction = BeginNewTransaction())
            {
                var entity = Get(key);
                if (entity != null)
                {
                    dbContext.Remove(entity);
                    dbContext.SaveChanges();
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
