﻿using System.Collections.Generic;
using System.Linq;
using CoreGallery.Db.Entities;
using CoreGallery.Extensions.Permissions;
using Microsoft.EntityFrameworkCore;

namespace CoreGallery.Db.Repositories.Impl
{
    public class RoleRepository : BaseCrudRepository<Role, RoleKey>, IRoleRepository
    {
        public RoleRepository(CoreGalleryContext dbContext) : base(dbContext)
        {
        }

        protected override void UpdateDbEntity(Role dbEntity, Role changedEntity)
        {
            dbEntity.Description = changedEntity.Description;
        }

        public IEnumerable<PermissionKey> GetPermissionsInRole(RoleKey role)
        {
            return dbContext.RolePermissionAssignments
                .Where(a => a.RoleKey == role)
                .Select(a => a.PermissionKey);
        }

        public override IEnumerable<Role> GetAll()
        {
            return dbContext.Roles
                .Include(r => r.RolePermissionAssignments)
                .OrderBy(r => r.Order);
        }

        public override Role Get(RoleKey key)
        {
            return GetAll()
                .FirstOrDefault(r => r.ID == key);
        }
    }
}
