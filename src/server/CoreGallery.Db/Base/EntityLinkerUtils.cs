﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CoreGallery.Db.Entities.Base;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoreGallery.Db.Base
{
    public static class EntityLinkerUtils
    {
        public static void SetupLinkedTable<TLink, TLinkA, TLinkAKey, TLinkB, TLinkBKey> (
            this EntityTypeBuilder<TLink> builder,
            Expression<Func<TLinkA, IEnumerable<TLink>>> navigationExpressionA,
            Expression<Func<TLinkB, IEnumerable<TLink>>> navigationExpressionB) 
            where TLink : class, ILinkerEntity<TLinkA, TLinkAKey, TLinkB, TLinkBKey> 
            where TLinkA : class, ISimpleEntity<TLinkAKey> 
            where TLinkB : class, ISimpleEntity<TLinkBKey>
        {
            builder.HasKey(e => new {e.AKey, e.BKey});
            builder
                .HasOne(e => e.A)
                .WithMany(navigationExpressionA)
                .HasForeignKey(l => l.AKey);
            builder
                .HasOne(l => l.B)
                .WithMany(navigationExpressionB)
                .HasForeignKey(l => l.BKey);
        }
    }
}
