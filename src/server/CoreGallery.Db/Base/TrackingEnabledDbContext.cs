﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoreGallery.Db.Entities.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CoreGallery.Db.Base
{
    public abstract class TrackingEnabledDbContext : DbContext
    {
        protected TrackingEnabledDbContext(DbContextOptions options) : base(options) { }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            ProcessTrackableEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            ProcessTrackableEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected virtual void ProcessTrackableEntities()
        {
            var now = DateTime.UtcNow;
            foreach (var newEntity in ChangeTracker.Entries().Where(e => e.State == EntityState.Added && e.Entity is ITrackedEntity))
            {
                newEntity.Property("CreatedAt").CurrentValue = now;
                newEntity.Property("UpdatedAt").CurrentValue = now;
            }

            foreach (var modifiedEntity in ChangeTracker.Entries().Where(e => e.State == EntityState.Modified && e.Entity is ITrackedEntity))
            {
                modifiedEntity.Property("UpdatedAt").CurrentValue = now;
            }

            foreach (var deletedEntry in ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted && e.Entity is INonDestructibleEntity))
            {
                deletedEntry.Property("DeletedAt").CurrentValue = now;
                deletedEntry.State = EntityState.Modified;
            }
        }
    }
}