﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoreGallery.Db.Base
{
    public static class EnumConversionExtension
    {
        public static void RegisterAsStringEnum<TEnum>(this PropertyBuilder<TEnum> property)
            where TEnum : Enum
        {
            property
                .HasConversion(
                    v => v.ToString(),
                    v => (TEnum) Enum.Parse(typeof(TEnum), v));
        }
    }
}