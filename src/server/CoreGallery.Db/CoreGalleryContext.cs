﻿using CoreGallery.Db.Base;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Entities.Relations;
using CoreGallery.Db.Seed;
using CoreGallery.Extensions.Permissions;
using Microsoft.EntityFrameworkCore;

namespace CoreGallery.Db
{
    public class CoreGalleryContext : TrackingEnabledDbContext
    {
        /* *** EF Repositories *** */
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RolePermissionAssignment> RolePermissionAssignments { get; set; }

        public DbSet<Album> Albums { get; set; }
        public DbSet<AlbumItem> AlbumItems { get; set; }

        /* *** Constructor *** */
        public CoreGalleryContext(DbContextOptions options) : base(options) { }

        /* *** Overrides *** */
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Setup Enum Conversions
            modelBuilder.Entity<Role>().Property(r => r.ID).RegisterAsStringEnum();
            modelBuilder.Entity<Permission>().Property(p => p.ID).RegisterAsStringEnum();

            // Setup Linked Tables
            modelBuilder.Entity<RolePermissionAssignment>()
                .SetupLinkedTable<RolePermissionAssignment, Role, RoleKey, Permission, PermissionKey>(
                    r => r.RolePermissionAssignments,
                    p  => p.PermissionRoleAssignments);

            // Albums: Unique name per user
            modelBuilder.Entity<Album>()
                .HasIndex(a => new { a.Name, a.UserID })
                .IsUnique();
            modelBuilder.Entity<Album>()
                .Property(a => a.IsPrivateAlbum)
                .HasConversion<int>();

            // Album Items: Unique name per album
            modelBuilder.Entity<AlbumItem>()
                .HasIndex(i => new { i.AlbumID, i.ItemName })
                .IsUnique();

            // Seed Data
            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permission>().HasData(DataSeed.PERMISSION_SEED);
            modelBuilder.Entity<Role>().HasData(DataSeed.ROLE_SEED);
            modelBuilder.Entity<RolePermissionAssignment>().HasData(DataSeed.ROLE_PERM_ASSIGN_SEED);
        }
    }
}
