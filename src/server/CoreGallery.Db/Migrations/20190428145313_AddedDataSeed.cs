﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreGallery.Db.Migrations
{
    public partial class AddedDataSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IsPrivateAlbum",
                table: "Albums",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "ID", "Description" },
                values: new object[,]
                {
                    { "Login", "Allows a user to login" },
                    { "ListUser", "Allows a user to list all registered users" },
                    { "ModifyUser", "Allows a user to modify registered users" },
                    { "ListAlbum", "Allows a user to list all public albums" },
                    { "ListPrivateAlbum", "Allows a user to list all private albums of other users" },
                    { "ModifyAlbum", "Allows a user to create a new album and modify own albums" },
                    { "ModifyForeignAlbum", "Allows a user to modify albums from other users" },
                    { "UploadImage", "Allows a user to upload images to an album" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "ID", "Description" },
                values: new object[,]
                {
                    { "SuperUser", "User with all privileges by default" },
                    { "Admin", "User with permissions required to manage the application" },
                    { "User", "Normal application user with permissions to handle most tasks" },
                    { "LockedUser", "User who is blocked from logging into the application" }
                });

            migrationBuilder.InsertData(
                table: "RolePermissionAssignments",
                columns: new[] { "AKey", "BKey" },
                values: new object[,]
                {
                    { "SuperUser", "Login" },
                    { "User", "ListAlbum" },
                    { "User", "Login" },
                    { "Admin", "UploadImage" },
                    { "Admin", "ModifyForeignAlbum" },
                    { "Admin", "ModifyAlbum" },
                    { "Admin", "ListPrivateAlbum" },
                    { "Admin", "ListAlbum" },
                    { "Admin", "ModifyUser" },
                    { "Admin", "ListUser" },
                    { "Admin", "Login" },
                    { "SuperUser", "UploadImage" },
                    { "SuperUser", "ModifyForeignAlbum" },
                    { "SuperUser", "ModifyAlbum" },
                    { "SuperUser", "ListPrivateAlbum" },
                    { "SuperUser", "ListAlbum" },
                    { "SuperUser", "ModifyUser" },
                    { "SuperUser", "ListUser" },
                    { "User", "ModifyAlbum" },
                    { "User", "UploadImage" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "Login" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ListUser" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ModifyUser" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ListAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ListPrivateAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ModifyAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "ModifyForeignAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "UploadImage" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "Login" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ListUser" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ModifyUser" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ListAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ListPrivateAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ModifyAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "ModifyForeignAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "Admin", "UploadImage" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "User", "Login" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "User", "ListAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "User", "ModifyAlbum" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "User", "UploadImage" });

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: "LockedUser");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "Login");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ListUser");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ModifyUser");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ListAlbum");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ListPrivateAlbum");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ModifyAlbum");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "ModifyForeignAlbum");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "UploadImage");

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: "SuperUser");

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: "Admin");

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: "User");

            migrationBuilder.AlterColumn<short>(
                name: "IsPrivateAlbum",
                table: "Albums",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
