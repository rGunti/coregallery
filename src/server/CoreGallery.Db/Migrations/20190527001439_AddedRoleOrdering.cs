﻿using CoreGallery.Extensions.DbExtensions.Description;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreGallery.Db.Migrations
{
    public partial class AddedRoleOrdering : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Roles",
                nullable: false,
                defaultValue: DescriptiveEnumUtils.DEFAULT_ORDER);

            migrationBuilder.UpdateData(
                "Roles",
                "Id",
                new object[] { "SuperUser", "Admin", "User", "LockedUser" },
                new[] { "ID", "Order" },
                new object[,]
                {
                    {"SuperUser", 1},
                    {"Admin", 2},
                    {"User", 100},
                    {"LockedUser", 9999}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "Roles");
        }
    }
}
