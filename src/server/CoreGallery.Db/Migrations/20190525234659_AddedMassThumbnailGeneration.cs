﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreGallery.Db.Migrations
{
    public partial class AddedMassThumbnailGeneration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "ID", "Description" },
                values: new object[,]
                {
                    { "TriggerMassThumbnailGeneration", "Allows a user to trigger mass-thumbnail generation" }
                });

            migrationBuilder.InsertData(
                table: "RolePermissionAssignments",
                columns: new[] { "AKey", "BKey" },
                values: new object[,]
                {
                    { "SuperUser", "TriggerMassThumbnailGeneration" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "SuperUser", "TriggerMassThumbnailGeneration" });
            
            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "ID",
                keyValue: "TriggerMassThumbnailGeneration");
        }
    }
}
