﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreGallery.Db.Migrations
{
    public partial class InitialSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    ID = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    ID = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RolePermissionAssignments",
                columns: table => new
                {
                    AKey = table.Column<string>(nullable: false),
                    BKey = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolePermissionAssignments", x => new { x.AKey, x.BKey });
                    table.ForeignKey(
                        name: "FK_RolePermissionAssignments_Roles_AKey",
                        column: x => x.AKey,
                        principalTable: "Roles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolePermissionAssignments_Permissions_BKey",
                        column: x => x.BKey,
                        principalTable: "Permissions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<string>(maxLength: 64, nullable: false),
                    PasswordHash = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    RoleID = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    UserID = table.Column<string>(nullable: false),
                    IsPrivateAlbum = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Albums_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlbumItems",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    AlbumID = table.Column<int>(nullable: false),
                    UserID = table.Column<string>(nullable: false),
                    ItemName = table.Column<string>(maxLength: 128, nullable: false),
                    ImageFormat = table.Column<string>(nullable: true),
                    ImageWidth = table.Column<int>(nullable: true),
                    ImageHeight = table.Column<int>(nullable: true),
                    BitDepth = table.Column<int>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    CopyrightNotice = table.Column<string>(nullable: true),
                    TakenAt = table.Column<DateTime>(nullable: true),
                    CameraMaker = table.Column<string>(nullable: true),
                    CameraModel = table.Column<string>(nullable: true),
                    PositionDescription = table.Column<string>(nullable: true),
                    PositionLatitude = table.Column<float>(nullable: true),
                    PositionLongitude = table.Column<float>(nullable: true),
                    PositionAltitude = table.Column<float>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlbumItems", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AlbumItems_Albums_AlbumID",
                        column: x => x.AlbumID,
                        principalTable: "Albums",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlbumItems_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlbumItems_UserID",
                table: "AlbumItems",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumItems_AlbumID_ItemName",
                table: "AlbumItems",
                columns: new[] { "AlbumID", "ItemName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Albums_UserID",
                table: "Albums",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Albums_Name_UserID",
                table: "Albums",
                columns: new[] { "Name", "UserID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RolePermissionAssignments_BKey",
                table: "RolePermissionAssignments",
                column: "BKey");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleID",
                table: "Users",
                column: "RoleID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlbumItems");

            migrationBuilder.DropTable(
                name: "RolePermissionAssignments");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
