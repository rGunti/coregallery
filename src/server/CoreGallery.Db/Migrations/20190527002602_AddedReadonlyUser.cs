﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreGallery.Db.Migrations
{
    public partial class AddedReadonlyUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "ID", "Description", "Order" },
                values: new object[,]
                {
                    { "ReadonlyUser", "Normal application user, but is not able to modify any content", 200 }
                });

            migrationBuilder.InsertData(
                table: "RolePermissionAssignments",
                columns: new[] { "AKey", "BKey" },
                values: new object[,]
                {
                    { "ReadonlyUser", "Login" },
                    { "ReadonlyUser", "ListAlbum" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "ReadonlyUser", "Login" });

            migrationBuilder.DeleteData(
                table: "RolePermissionAssignments",
                keyColumns: new[] { "AKey", "BKey" },
                keyValues: new object[] { "ReadonlyUser", "ListAlbum" });

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: "ReadonlyUser");
        }
    }
}
