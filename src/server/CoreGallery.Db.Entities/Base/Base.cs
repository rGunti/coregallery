﻿using System;

namespace CoreGallery.Db.Entities.Base
{
    public interface IEntity { }

    public interface ISimpleEntity<TPrimaryKey> : IEntity
    {
        TPrimaryKey ID { get; set; }
    }

    public interface ILinkerEntity<TEntityA, TEntityAKey, TEntityB, TEntityBKey>
        where TEntityA : ISimpleEntity<TEntityAKey>
        where TEntityB : ISimpleEntity<TEntityBKey>
    {
        TEntityA A { get; set; }
        TEntityAKey AKey { get; set; }

        TEntityB B { get; set; }
        TEntityBKey BKey { get; set; }
    }

    public interface ITrackedEntity
    {
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }

    public interface INonDestructibleEntity
    {
        DateTime? DeletedAt { get; set; }
    }
}