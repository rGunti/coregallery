﻿using System.Collections.Generic;

namespace CoreGallery.Db.Entities.Base
{
    public abstract class BaseLinkerEntity<TSelf, TEntityA, TEntityAKey, TEntityB, TEntityBKey>
        : ILinkerEntity<TEntityA, TEntityAKey, TEntityB, TEntityBKey>
        where TSelf : class, ILinkerEntity<TEntityA, TEntityAKey, TEntityB, TEntityBKey>
        where TEntityA : ISimpleEntity<TEntityAKey>
        where TEntityB : ISimpleEntity<TEntityBKey>
    {
        public TEntityA A { get; set; }
        public TEntityAKey AKey { get; set; }
        public TEntityB B { get; set; }
        public TEntityBKey BKey { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BaseLinkerEntity<TSelf, TEntityA, TEntityAKey, TEntityB, TEntityBKey>) obj);
        }

        protected bool Equals(BaseLinkerEntity<TSelf, TEntityA, TEntityAKey, TEntityB, TEntityBKey> other)
        {
            return EqualityComparer<TEntityAKey>.Default.Equals(AKey, other.AKey) && EqualityComparer<TEntityBKey>.Default.Equals(BKey, other.BKey);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<TEntityAKey>.Default.GetHashCode(AKey) * 397) ^ EqualityComparer<TEntityBKey>.Default.GetHashCode(BKey);
            }
        }
    }
}