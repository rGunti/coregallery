﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Entities.Relations;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Entities
{
    public class Permission : ISimpleEntity<PermissionKey>
    {
        [Key]
        [StringLength(64)]
        public PermissionKey ID { get; set; }
        public string Description { get; set; }

        /* *** Foreign Key Refs *** */
        public ICollection<RolePermissionAssignment> PermissionRoleAssignments { get; set; }
    }
}