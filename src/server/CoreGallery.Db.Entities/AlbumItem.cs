﻿using System;
using System.ComponentModel.DataAnnotations;
using CoreGallery.Db.Entities.Base;

namespace CoreGallery.Db.Entities
{
    public class AlbumItem : ISimpleEntity<int>, ITrackedEntity
    {
        public int ID { get; set; }
    
        public int AlbumID { get; set; }
        public Album Album { get; set; }

        [Required]
        public string UserID { get; set; }
        public User Uploader { get; set; }
    
        [Required]
        [StringLength(128)]
        public string ItemName { get; set; }

        /* ** Technical Image Properties ** */
        /* * Image Format * */
        public string ImageFormat { get; set; }
        public int? ImageWidth { get; set; }
        public int? ImageHeight { get; set; }
        public int? BitDepth { get; set; }

        /* * Author * */
        public string Author { get; set; }
        public string CopyrightNotice { get; set; }

        /* * Photo detail * */
        public DateTime? TakenAt { get; set; }

        /* * Camera detail * */
        public string CameraMaker { get; set; }
        public string CameraModel { get; set; }

        /* * Location * */
        public string PositionDescription { get; set; }
        public float? PositionLatitude { get; set; }
        public float? PositionLongitude { get; set; }
        public float? PositionAltitude { get; set; }
    
        /* ** Tracking properties ** */
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        /* ** Helper Property ** */
        public DateTime SortDate => TakenAt ?? CreatedAt;
    }
}