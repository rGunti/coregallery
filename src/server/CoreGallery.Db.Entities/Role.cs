﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Entities.Relations;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Entities
{
    public class Role : ISimpleEntity<RoleKey>
    {
        [Key]
        [StringLength(64)]
        public RoleKey ID { get; set; }

        public string Description { get; set; }

        public int Order { get; set; }

        /* *** Foreign Key Refs *** */
        public ICollection<User> RoleMembers { get; set; }
        public ICollection<RolePermissionAssignment> RolePermissionAssignments { get; set; }
    }
}
