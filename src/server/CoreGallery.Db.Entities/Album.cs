﻿using System;
using System.ComponentModel.DataAnnotations;
using CoreGallery.Db.Entities.Base;

namespace CoreGallery.Db.Entities
{
    public class Album : ISimpleEntity<int>, ITrackedEntity
    {
        public int ID { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [Required]
        public string UserID { get; set; }
        public User Owner { get; set; }

        public bool IsPrivateAlbum { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}