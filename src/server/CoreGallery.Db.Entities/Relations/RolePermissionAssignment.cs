﻿using System.ComponentModel.DataAnnotations.Schema;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Extensions.Permissions;

namespace CoreGallery.Db.Entities.Relations
{
    public class RolePermissionAssignment : BaseLinkerEntity<RolePermissionAssignment, Role, RoleKey, Permission, PermissionKey>
    {
        public RolePermissionAssignment() { }

        public RolePermissionAssignment(RoleKey role, PermissionKey permission)
        {
            RoleKey = role;
            PermissionKey = permission;
        }

        /* *** Convenience Alias Properties for easy to read code *** */
        [NotMapped]
        public RoleKey RoleKey
        {
            get => AKey;
            set => AKey = value;
        }

        [NotMapped]
        public PermissionKey PermissionKey
        {
            get => BKey;
            set => BKey = value;
        }
    }
}