﻿using System;
using System.ComponentModel.DataAnnotations;
using CoreGallery.Db.Entities.Base;

namespace CoreGallery.Db.Entities
{
    public class User : ISimpleEntity<string>, ITrackedEntity, INonDestructibleEntity
    {
        [Key]
        [StringLength(64, MinimumLength = 3)]
        public string ID { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }
        [Required]
        public byte[] PasswordSalt { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
