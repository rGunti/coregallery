﻿using CoreGallery.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CoreGallery.Setup
{
    public class CoreGalleryContextFactory : IDesignTimeDbContextFactory<CoreGalleryContext>
    {
        public CoreGalleryContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CoreGalleryContext>();
            optionsBuilder.UseMySQL("server=localhost;database=dev-coregallery;user=root;password=toor");
            return new CoreGalleryContext(optionsBuilder.Options);
        }
    }
}