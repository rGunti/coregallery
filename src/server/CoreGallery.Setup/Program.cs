﻿using System;
using CoreGallery.Db;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.Infraestructure;

namespace CoreGallery.Setup
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Constructing context ...");
            using (var dbContext = new CoreGalleryContextFactory().CreateDbContext(args))
            {
                Console.WriteLine("DbContext constructed!");
            }
            Console.WriteLine("Completed!");
        }
    }
}
