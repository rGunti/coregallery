﻿namespace CoreGallery.Extensions.Paging
{
    public interface INeighborhood<T>
    {
        T Current { get; }

        T Previous { get; }
        T Next { get; }
    }

    public class Neighborhood<T> : INeighborhood<T>
    {
        public Neighborhood() { }
        public Neighborhood(T current, T previous = default(T), T next = default(T))
        {
            Current = current;
            Previous = previous;
            Next = next;
        }

        public T Current { get; set; }
        public T Previous { get; set; }
        public T Next { get; set; }
    }
}