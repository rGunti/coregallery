﻿using System.Collections.Generic;

namespace CoreGallery.Extensions.Paging
{
    public interface IPagedResult<T>
    {
        IEnumerable<T> Items { get; }
        int TotalItems { get; }
    }

    public class PagedResult<T> : 
        IPagedResult<T>
    {
        public PagedResult() { }
        public PagedResult(IEnumerable<T> items, int totalItems)
        {
            Items = items;
            TotalItems = totalItems;
        }

        public IEnumerable<T> Items { get; set; }
        public int TotalItems { get; set; }
    }
}
