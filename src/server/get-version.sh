#!/bin/bash
_ver=$(cat 'Directory.Build.props' | grep 'Version' | tr -d '[:space:]' | sed "s/<Version>//g" | sed "s/<\/Version>//g")
echo "##teamcity[buildNumber '${_ver}']" > version.txt
