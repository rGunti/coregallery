﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CoreGallery.Extensions.Permissions.PermissionAssignment
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AssignPermissionsAttribute : Attribute
    {
        public AssignPermissionsAttribute()
        {
            Permissions = new List<PermissionKey>().AsReadOnly();
        }
        public AssignPermissionsAttribute(params PermissionKey[] permissions)
        {
            Permissions = permissions.ToList().AsReadOnly();
        }

        public IReadOnlyList<PermissionKey> Permissions { get; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class AssignAllPermissionsAttribute : Attribute
    {
        public AssignAllPermissionsAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class RemoveAllPermissionsAttribute : Attribute
    {
        public RemoveAllPermissionsAttribute()
        {
        }
    }

    public static class PermissionAssignmentAttributeExtension
    {
        public static ISet<PermissionKey> GetAssignedPermissions(this RoleKey role)
        {
            var fieldInfo = role.GetType().GetField(role.ToString());

            var removeAllPermissions =
                fieldInfo.GetCustomAttributes<RemoveAllPermissionsAttribute>().Any();
            if (removeAllPermissions)
            {
                return new HashSet<PermissionKey>();
            }

            var assignAllPermissions =
                fieldInfo.GetCustomAttributes<AssignAllPermissionsAttribute>().Any();
            if (assignAllPermissions)
            {
                return new HashSet<PermissionKey>(
                    Enum.GetValues(typeof(PermissionKey)).Cast<PermissionKey>());
            }

            var assignmentAttributes = fieldInfo.GetCustomAttributes<AssignPermissionsAttribute>().ToArray();
            var set = new HashSet<PermissionKey>();
            foreach (var permissionAssignmentAttribute in assignmentAttributes)
            {
                foreach (var permissionKey in permissionAssignmentAttribute.Permissions)
                    set.Add(permissionKey);
            }
            return set;
        }
    }
}
