﻿
using CoreGallery.Extensions.DbExtensions.Description;

namespace CoreGallery.Extensions.Permissions
{
    [DescriptiveEnumEnforcement(DescriptiveEnumEnforcementStyle.DefaultToEmptyString)]
    public enum PermissionKey
    {
        [Description("Allows a user to login")]
        Login,

        [Description("Allows a user to list all registered users")]
        ListUser,
        [Description("Allows a user to modify registered users")]
        ModifyUser,

        [Description("Allows a user to list all public albums")]
        ListAlbum,
        [Description("Allows a user to list all private albums of other users")]
        ListPrivateAlbum,
        [Description("Allows a user to create a new album and modify own albums")]
        ModifyAlbum,
        [Description("Allows a user to modify albums from other users")]
        ModifyForeignAlbum,

        [Description("Allows a user to upload images to an album")]
        UploadImage,

        [Description("Allows a user to trigger mass-thumbnail generation")]
        TriggerMassThumbnailGeneration
    }
}
