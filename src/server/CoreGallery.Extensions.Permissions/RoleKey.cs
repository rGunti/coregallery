﻿using CoreGallery.Extensions.DbExtensions.Description;
using CoreGallery.Extensions.Permissions.PermissionAssignment;

namespace CoreGallery.Extensions.Permissions
{
    [DescriptiveEnumEnforcement(DescriptiveEnumEnforcementStyle.DefaultToEmptyString)]
    public enum RoleKey
    {
        [Description("User with all privileges by default", Order = 1)]
        [AssignAllPermissions]
        SuperUser,

        [Description("User with permissions required to manage the application", Order = 2)]
        [AssignPermissions(
            PermissionKey.Login,

            PermissionKey.ListUser,
            PermissionKey.ModifyUser,

            PermissionKey.ListAlbum,
            PermissionKey.ListPrivateAlbum,
            PermissionKey.ModifyAlbum,
            PermissionKey.ModifyForeignAlbum,

            PermissionKey.UploadImage
        )]
        Admin,

        [Description("Normal application user with permissions to handle most tasks", Order = 100)]
        [AssignPermissions(
            PermissionKey.Login,

            PermissionKey.ListAlbum,
            PermissionKey.ModifyAlbum,
            
            PermissionKey.UploadImage
        )]
        User,

        [Description("Normal application user, but is not able to modify any content", Order = 200)]
        [AssignPermissions(
            PermissionKey.Login,
            PermissionKey.ListAlbum)]
        ReadonlyUser,

        [Description("User who is blocked from logging into the application", Order = 9999)]
        [RemoveAllPermissions]
        LockedUser
    }
}
