﻿using System;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/utils")]
    [ApiController]
    [Authorize]
    public class UtilsController : ControllerBase
    {
        private readonly IAlbumItemFileService fileService;
        private readonly IExifDataService exifDataService;

        public UtilsController(
            IAlbumItemFileService fileService,
            IExifDataService exifDataService)
        {
            this.fileService = fileService;
            this.exifDataService = exifDataService;
        }

        [HttpPost("read-exif-data")]
        [Permission(PermissionKey.UploadImage)]
        public IActionResult ReadExifData(IFormFile imageFile)
        {
            if (imageFile.Length <= 0 || !fileService.IsMimeTypeAllowed(imageFile.ContentType))
                return BadRequest();

            var exifData = exifDataService.ReadExifDataFromFile(imageFile);
            exifData.FileContent = null;
            exifData.AlbumID = -1;
            exifData.ID = -1;
            exifData.CreatedAt = DateTime.Now;
            exifData.UpdatedAt = DateTime.Now;
            exifData.UserID = HttpContext.User.Identity.Name;
            return Ok(exifData);
        }
    }
}