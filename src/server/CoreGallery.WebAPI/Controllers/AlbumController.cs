﻿using System.Collections.Generic;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/album")]
    [ApiController]
    [Authorize]
    [Permission(PermissionKey.ListAlbum)]
    public class AlbumController : BaseCrudController<AlbumDTO, int, IAlbumService>
    {
        private readonly IUserService userService;

        public AlbumController(
            IAlbumService crudService,
            IUserService userService)
            : base(crudService)
        {
            this.userService = userService;
        }

        [HttpGet]
        public override IActionResult GetAll()
        {
            if (!userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ListPrivateAlbum))
            {
                return Ok(crudService.GetAllWithoutPrivateReadPermission(AuthenticatedUser));
            }

            return base.GetAll();
        }

        [HttpGet("{key}")]
        [Permission(PermissionKey.ListAlbum)]
        public override IActionResult Get(int key)
        {
            var album = crudService.Get(key);
            if (album == null) return NotFound();
            if (album.IsPrivateAlbum &&
                album.UserID != AuthenticatedUser &&
                !userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ListPrivateAlbum))
            {
                return Forbid();
            }
            return Ok(album);
        }

        [HttpPost]
        [Permission(PermissionKey.ModifyAlbum)]
        public override IActionResult Post(AlbumDTO item)
        {
            var username = HttpContext.User.Identity.Name;

            if (!string.IsNullOrWhiteSpace(item.UserID)
                && item.UserID != username)
            {
                return BadRequest();
            }

            item.UserID = username;
            return base.Post(item);
        }

        [HttpPut("{key}")]
        [Permission(PermissionKey.ModifyAlbum)]
        public override IActionResult Put(int key, AlbumDTO item)
        {
            var album = crudService.Get(key);
            if (album == null) return NotFound();
            if (album.IsPrivateAlbum &&
                album.UserID != AuthenticatedUser &&
                !userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ModifyForeignAlbum))
            {
                return Forbid();
            }
            return base.Put(key, item);
        }

        [HttpDelete("{key}")]
        [Permission(PermissionKey.ModifyAlbum)]
        public override IActionResult Delete(int key)
        {
            var album = crudService.Get(key);
            if (album == null) return NotFound();
            if (album.IsPrivateAlbum &&
                album.UserID != AuthenticatedUser &&
                !userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ModifyForeignAlbum))
            {
                return Forbid();
            }
            return base.Delete(key);
        }
    }
}
