﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.Responses;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authentication;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/user")]
    [ApiController]
    [Authorize]
    public class UserController : BaseCrudController<UserDTO, string, IUserService>
    {
        private static readonly IResponse RES_INVALID_PW
            = new SimpleErrorResponse("Invalid username or password");

        public UserController(
            IUserService crudService)
            : base(crudService)
        {
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] SimpleLoginRequestBody simpleUser)
        {
            var user = crudService.Authenticate(simpleUser.Username, simpleUser.Password);
            if (user == null)
                return BadRequest(RES_INVALID_PW);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(AuthenticationSetup.CRYPTO_KEY);
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.ID),
                    new Claim(ClaimTypes.Role, user.Role.ID.ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim("permissions", string.Join(",", user.Role.Permissions.Select(p => p.ToString())))
                }),
                //Expires = DateTime.UtcNow.AddHours(12),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key), 
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescription);
            var tokenString = tokenHandler.WriteToken(token);

            user.Token = tokenString;
            return Ok(user);
        }

        [Permission(PermissionKey.ListUser)]
        public override IActionResult GetAll() => base.GetAll();

        [Permission(PermissionKey.ListUser)]
        public override IActionResult Get(string key) => base.Get(key);

        [Permission(PermissionKey.ListUser)]
        [Permission(PermissionKey.ModifyUser)]
        public override IActionResult Post(UserDTO item) => base.Post(item);

        [Permission(PermissionKey.ListUser)]
        [Permission(PermissionKey.ModifyUser)]
        public override IActionResult Put(string key, UserDTO item) => base.Put(key, item);

        [Permission(PermissionKey.ListUser)]
        [Permission(PermissionKey.ModifyUser)]
        public override IActionResult Delete(string key) => base.Delete(key);

        [Permission(PermissionKey.ListUser)]
        [Permission(PermissionKey.ModifyUser)]
        [HttpPost("{key}/reactivate")]
        public IActionResult Reactivate(string key)
        {
            var newUser = crudService.ReactivateUser(key);
            if (newUser != null)
            {
                return Ok(newUser);
            }
            else
            {
                return BadRequest();
            }
        }

        [Permission(PermissionKey.ListUser)]
        [Permission(PermissionKey.ModifyUser)]
        [HttpPost("{key}/reset")]
        public IActionResult ResetPassword(string key)
        {
            var newUser = crudService.ResetPassword(key);
            if (newUser != null)
            {
                return Ok(newUser);
            }
            else
            {
                return BadRequest();
            }
        }
    }

    public class SimpleLoginRequestBody
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}