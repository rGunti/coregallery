﻿using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        // GET: api/Health
        [HttpGet]
        public string Get()
        {
            return "I am alive!";
        }
    }
}
