﻿using System;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/role")]
    [ApiController]
    [Authorize]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService roleService;

        public RoleController(
            IRoleService roleService)
        {
            this.roleService = roleService;
        }

        [HttpGet]
        [Permission(PermissionKey.ListUser)]
        public IActionResult GetAllRoles()
        {
            return Ok(roleService.GetAll());
        }

        [HttpGet("{role}")]
        public IActionResult GetRole(string role)
        {
            if (Enum.TryParse<RoleKey>(role, out var roleKey))
            {
                return Ok(roleService.Get(roleKey));
            }

            return NotFound();
        }
    }
}