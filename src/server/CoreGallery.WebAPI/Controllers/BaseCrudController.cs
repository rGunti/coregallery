﻿using System;
using System.Collections.Generic;
using CoreGallery.WebAPI.DTOs.Base;
using CoreGallery.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    public abstract class BaseCrudController<TDto, TKey, TCrudService>
        : ControllerBase
        where TDto : ISimpleDTO<TKey>
        where TCrudService : ICrudService<TDto, TKey>
    {
        protected readonly TCrudService crudService;

        protected BaseCrudController(TCrudService crudService)
        {
            this.crudService = crudService;
        }

        /// <summary>
        /// QoL property, returns the username of the authenticated user
        /// </summary>
        protected string AuthenticatedUser => HttpContext.User.Identity.Name;

        [HttpGet]
        public virtual IActionResult GetAll()
        {
            return Ok(crudService.GetAll());
        }

        [HttpGet("{key}")]
        public virtual IActionResult Get(TKey key)
        {
            var item = crudService.Get(key);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost]
        public virtual IActionResult Post([FromBody] TDto item)
        {
            try
            {
                var newItem = crudService.Create(item);
                return Ok(newItem);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{key}")]
        public virtual IActionResult Put(TKey key, [FromBody] TDto item)
        {
            try
            {
                if (item == null || !Equals(key, item.ID)) return BadRequest();
                crudService.Update(item);
                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{key}")]
        public virtual IActionResult Delete(TKey key)
        {
            try
            {
                crudService.Delete(key);
                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
