﻿using System.Threading;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/admin/tools")]
    [ApiController]
    [Authorize]
    public class AdminToolsController : ControllerBase
    {
        private readonly IAlbumService albumService;
        private readonly IAlbumItemService albumItemService;
        private readonly IAlbumItemFileService albumItemFileService;
        private readonly ILogger<AdminToolsController> logger;

        public AdminToolsController(
            ILogger<AdminToolsController> logger,
            IAlbumService albumService,
            IAlbumItemService albumItemService,
            IAlbumItemFileService albumItemFileService)
        {
            this.logger = logger;
            this.albumService = albumService;
            this.albumItemService = albumItemService;
            this.albumItemFileService = albumItemFileService;
        }

        [HttpPost("thumbnail-generator")]
        [Permission(PermissionKey.TriggerMassThumbnailGeneration)]
        public IActionResult TriggerThumbnailGenerator()
        {
            foreach (var album in albumService.GetAll())
            {
                foreach (var albumItem in albumItemService.GetItemsFromAlbum(album.ID))
                {
                    ThreadPool.QueueUserWorkItem(o2 =>
                    {
                        logger.LogDebug($"Picking up thumbnail generation for {album.ID}/{albumItem.ID} ...");
                        albumItemFileService.GetThumbnailImage(albumItem, -1, -1);
                    });
                    logger.LogInformation($"Enqueued thumbnail generation for {album.ID}/{albumItem.ID}");
                }
                logger.LogInformation($"Enqueued thumbnail generation for Album {album.ID}");
            }
            return Accepted();
        }
    }
}