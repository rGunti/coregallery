﻿using System.Runtime.InteropServices;
using CoreGallery.Db.Entities;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreGallery.WebAPI.Controllers
{
    [Route("api/album/{albumId}/item")]
    [ApiController]
    [Authorize]
    public class AlbumItemController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IAlbumService albumService;
        private readonly IAlbumItemService albumItemService;
        private readonly IAlbumItemFileService albumItemFileService;

        public AlbumItemController(
            IUserService userService,
            IAlbumService albumService,
            IAlbumItemService albumItemService,
            IAlbumItemFileService albumItemFileService)
        {
            this.userService = userService;
            this.albumService = albumService;
            this.albumItemService = albumItemService;
            this.albumItemFileService = albumItemFileService;    
        }

        /// <summary>
        /// QoL property, returns the username of the authenticated user
        /// </summary>
        private string AuthenticatedUser => HttpContext.User.Identity.Name;

        /// <summary>
        /// Returns <code>true</code> if the logged in user is not allowed to read from the given album
        /// </summary>
        private bool DenyReadAccessToPrivateAlbumRequired(AlbumDTO album)
        {
            return album.IsPrivateAlbum &&
                   album.UserID != AuthenticatedUser &&
                   !userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ListPrivateAlbum);
        }

        /// <summary>
        /// Returns <code>true</code> if the logged in user is not allowed to read from or write to the given album
        /// </summary>
        private bool DenyWriteAccessToPrivateAlbumRequired(AlbumDTO album)
        {
            return DenyReadAccessToPrivateAlbumRequired(album) ||
                   album.UserID != AuthenticatedUser &&
                   !userService.UserHasPermissions(AuthenticatedUser, PermissionKey.ModifyForeignAlbum);
        }

        [HttpGet]
        [Permission(PermissionKey.ListAlbum)]
        public IActionResult GetItemsInAlbum(
            int albumId,
            [FromQuery] int page = 0)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyReadAccessToPrivateAlbumRequired(album))
                return Forbid();

            return Ok(albumItemService.GetPagesItemsFromAlbum(albumId, page));
        }

        [HttpGet("{id}")]
        [Permission(PermissionKey.ListAlbum)]
        public IActionResult Get(int albumId, int id)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyReadAccessToPrivateAlbumRequired(album))
                return Forbid();

            var albumItem = albumItemService.GetItem(albumId, id);
            if (albumItem == null) return NotFound();

            return Ok(albumItem);
        }

        [HttpGet("{id}/neighborhood")]
        [Permission(PermissionKey.ListAlbum)]
        public IActionResult GetNeighborhood(int albumId, int id)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyReadAccessToPrivateAlbumRequired(album))
                return Forbid();

            return Ok(albumItemService.GetNeighborhoodOfAlbumItem(albumId, id));
        }

        [HttpGet("{id}/image")]
        [Permission(PermissionKey.ListAlbum)]
        public IActionResult GetImage(int albumId, int id)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyReadAccessToPrivateAlbumRequired(album))
                return Forbid();

            var albumItem = albumItemService.GetItem(albumId, id);
            if (albumItem == null) return NotFound();

            var fileStream = albumItemFileService.GetImage(albumItem);
            if (fileStream != null)
            {
                return File(fileStream, albumItem.ImageFormat);
            }
            return StatusCode(500);
        }

        [HttpGet("{id}/image/thumb")]
        [Permission(PermissionKey.ListAlbum)]
        public IActionResult GetImageThumbnail(
            int albumId, int id/*,
            [FromQuery] int width = -1,
            [FromQuery] int height = -1*/)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyReadAccessToPrivateAlbumRequired(album))
                return Forbid();

            var albumItem = albumItemService.GetItem(albumId, id);
            if (albumItem == null) return NotFound();

            var fileStream = albumItemFileService.GetThumbnailImage(albumItem, -1, -1);
            if (fileStream != null)
            {
                return File(fileStream, "image/jpg");
            }
            return StatusCode(500);
        }

        [HttpPost]
        [Permission(PermissionKey.UploadImage)]
        public IActionResult Post(int albumId, IFormFile imageFile)
        {
            if (imageFile.Length <= 0
                || !albumItemFileService.IsMimeTypeAllowed(imageFile.ContentType))
                return BadRequest();

            var album = albumService.Get(albumId);
            if (album == null) return NotFound();

            if (DenyWriteAccessToPrivateAlbumRequired(album))
                return Forbid();

            var item = albumItemService.Create(
                imageFile,
                AuthenticatedUser,
                albumId);

            return Ok(item);
        }

        [HttpPut("{id}")]
        [Permission(PermissionKey.ModifyAlbum)]
        public IActionResult Put(int albumId, int id, [FromBody] AlbumItemDTO updatedItem)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();
            var albumItem = albumItemService.GetItem(albumId, id);
            if (albumItem == null) return NotFound();

            if (DenyWriteAccessToPrivateAlbumRequired(album))
                return Forbid();

            updatedItem.AlbumID = albumId;
            albumItemService.Update(updatedItem);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Permission(PermissionKey.ModifyAlbum)]
        public IActionResult Delete(int albumId, int id)
        {
            var album = albumService.Get(albumId);
            if (album == null) return NotFound();
            var albumItem = albumItemService.GetItem(albumId, id);
            if (albumItem == null) return NotFound();

            if (DenyWriteAccessToPrivateAlbumRequired(album))
                return Forbid();

            albumItemService.Delete(albumItem);
            return Ok();
        }
    }
}
