﻿using Microsoft.Extensions.DependencyInjection;

namespace CoreGallery.WebAPI.Services
{
    public static class ServiceRegistrationExtension
    {
        public static IServiceCollection AddCoreGalleryDtoServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IAlbumService, AlbumService>()
                .AddScoped<IAlbumItemService, AlbumItemService>()
                .AddScoped<IUserService, UserService>()
                .AddScoped<IRoleService, RoleService>()
                .AddScoped<IPermissionService, PermissionService>()

                .AddScoped<IAlbumItemFileService, ImageSharpAlbumItemFileService>()
                .AddScoped<IExifDataService, ExifDataService>()
            ;
        }
    }
}
