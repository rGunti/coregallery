﻿using System.Collections.Generic;
using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories;
using CoreGallery.WebAPI.DTOs;

namespace CoreGallery.WebAPI.Services
{
    public interface IAlbumService
        : ICrudService<AlbumDTO, int>
    {
        IEnumerable<AlbumDTO> GetAllWithoutPrivateReadPermission(string username);
    }

    public class AlbumService : 
        BaseCrudDtoService<IAlbumRepository, Album, int, AlbumDTO, int>,
        IAlbumService
    {
        public AlbumService(
            IAlbumRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        public IEnumerable<AlbumDTO> GetAllWithoutPrivateReadPermission(string username)
        {
            var albums = repository.GetAlbumsForUser(username);
            return mapper.Map<IList<AlbumDTO>>(albums);
        }
    }
}