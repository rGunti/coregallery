﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using CoreGallery.WebAPI.DTOs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CoreGallery.WebAPI.Services
{
    public interface IAlbumItemFileService : IFileService
    {
        Stream GetImage(AlbumItemDTO item);
        Stream GetThumbnailImage(AlbumItemDTO item, int width = 320, int height = 240);
        bool StoreImage(AlbumItemDTO item, byte[] content);
        bool StoreImage(AlbumItemDTO item, Stream readStream);
        bool DeleteImage(AlbumItemDTO item);

        bool IsMimeTypeAllowed(string mimeType);
    }

    public class AlbumItemFileService :
        BaseFileService,
        IAlbumItemFileService
    {
        protected static readonly string[] ALLOWED_TYPES = new[]
        {
            "image/jpeg",
            "image/jpg"
        };

        /* ** Experimental Features ** */
        protected readonly bool runGcWhenThumbnailIsGenerated = true;

        protected readonly string baseFilePath;
        protected readonly IExifDataService exifDataService;
        protected readonly IAlbumService albumService;

        public AlbumItemFileService(
            ILogger<AlbumItemFileService> logger,
            IConfiguration configuration,
            IAlbumService albumService,
            IExifDataService exifDataService)
            : base(logger)
        {
            this.exifDataService = exifDataService;
            this.albumService = albumService;

            this.baseFilePath = configuration.GetSection("StoragePath").Value;
            if (!Directory.Exists(baseFilePath))
            {
                logger.LogInformation("Creating data directory ...");
                Directory.CreateDirectory(baseFilePath);
            }
        }

        protected virtual string BuildFilePath(int albumId, int itemId)
        {
            return Path.Combine(baseFilePath, "album", $"{albumId}", $"{itemId}.JPG");
        }

        protected virtual string BuildThumbnailFilePath(int albumId, int itemId, int width = 320, int height = 240)
        {
            var stringWidth = width < 0 ? "AUTO" : width.ToString();
            var stringHeight = height < 0 ? "AUTO" : height.ToString();
            var res = $"{stringWidth}x{stringHeight}";
            return Path.Combine(baseFilePath, "thumbs", $"{albumId}", res, $"{itemId}.{res}.JPG");
        }

        protected virtual void EnsureDirectoryForFile(string path)
        {
            var baseDir = Path.GetDirectoryName(path);
            if (!Directory.Exists(baseDir))
            {
                Directory.CreateDirectory(baseDir);
            }
        }

        protected virtual string BuildAndEnsureFilePath(int albumId, int itemId)
        {
            var path = BuildFilePath(albumId, itemId);
            EnsureDirectoryForFile(path);
            return path;
        }

        public Stream GetImage(AlbumItemDTO item)
        {
            return ReadFileAsStream(BuildFilePath(item.AlbumID, item.ID));
        }

        protected static readonly RotateFlipType[] LANDSCAPE_ORIENTATIONS = new[]
        {
            RotateFlipType.RotateNoneFlipNone, RotateFlipType.RotateNoneFlipX, RotateFlipType.RotateNoneFlipXY, RotateFlipType.RotateNoneFlipY,
            RotateFlipType.Rotate180FlipNone, RotateFlipType.Rotate180FlipX, RotateFlipType.Rotate180FlipXY, RotateFlipType.Rotate180FlipY
        };

        public virtual Stream GetThumbnailImage(AlbumItemDTO item, int width = 320, int height = 240)
        {
            var thumbnailPath = BuildThumbnailFilePath(item.AlbumID, item.ID, width, height);
            if (!File.Exists(thumbnailPath))
            {
                EnsureDirectoryForFile(thumbnailPath);
                BuildThumbnailImage(item, width, height, thumbnailPath);
            }
            return ReadFileAsStream(thumbnailPath);
        }

        protected virtual Size CalculateThumbnailSize(
            int sourceWidth, int sourceHeight,
            int thumbnailWidth, int thumbnailHeight)
        {
            float sourceAspectRatio = (float)sourceWidth / (float)sourceHeight;
            if (thumbnailWidth < 0 && thumbnailHeight >= 0)
            {
                thumbnailWidth = (int)(thumbnailHeight * sourceAspectRatio);
            }
            else if (thumbnailHeight < 0 && thumbnailWidth >= 0)
            {
                thumbnailHeight = (int)(thumbnailWidth / sourceAspectRatio);
            }
            else
            {
                thumbnailWidth = 320;
                thumbnailHeight = 240;
            }

            return new Size(thumbnailWidth, thumbnailHeight);
        }

        protected virtual void BuildThumbnailImage(AlbumItemDTO item, int width, int height, string thumbnailPath)
        {
            logger.LogInformation($"Generating thumbnail for {item.AlbumID}/{item.ID} ({Path.GetFileName(thumbnailPath)}) ...");

            if (width == -1 && height == -1)
            {
                height = 240;
            }

            var originalStream = GetImage(item);
            var sourceImage = new Bitmap(originalStream);

            // Determine orientation
            var orientation = exifDataService.GetRotationFromExifData(originalStream);
            var isLandscape = LANDSCAPE_ORIENTATIONS.Contains(orientation);

            // Determine exact thumb width / height
            int sourceWidth = isLandscape ? sourceImage.Width : sourceImage.Height;
            int sourceHeight = isLandscape ? sourceImage.Height : sourceImage.Width;
            var thumbnailSize = CalculateThumbnailSize(
                sourceWidth, sourceHeight,
                width, height);
            width = thumbnailSize.Width;
            height = thumbnailSize.Height;

            // Building the thumbnail OTF
            try
            {
                using (var thumbMap = new Bitmap(width, height))
                {
                    float scale = Math.Min(
                        (float)width / sourceWidth,
                        (float)height / sourceHeight);
                    var scaleWidth = (int)(sourceWidth * scale);
                    var scaleHeight = (int)(sourceHeight * scale);

                    thumbMap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);
                    using (var graphics = Graphics.FromImage(thumbMap))
                    {
                        graphics.SmoothingMode = SmoothingMode.AntiAlias;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        graphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 0, 0)),
                            new RectangleF(0, 0, width, height));

                        sourceImage.RotateFlip(orientation);
                        graphics.DrawImage(
                            sourceImage,
                            ((int)width - scaleWidth) / 2,
                            ((int)height - scaleHeight) / 2,
                            scaleWidth, scaleHeight);

                        thumbMap.Save(thumbnailPath, ImageFormat.Jpeg);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                logger.LogCritical(ex, $"Failed to generate thumbnail for {item.AlbumID}/{item.ID}");
            }
            finally
            {
                sourceImage.Dispose();
                if (runGcWhenThumbnailIsGenerated)
                {
                    logger.LogDebug("Running GC to cleanup memory ...");
                    GC.Collect();
                }
            }
        }

        public virtual bool StoreImage(AlbumItemDTO item, byte[] content)
        {
            var path = BuildAndEnsureFilePath(item.AlbumID, item.ID);
            if (File.Exists(path)) return false;
            return WriteFile(path, content);
        }

        public virtual bool StoreImage(AlbumItemDTO item, Stream readStream)
        {
            var path = BuildAndEnsureFilePath(item.AlbumID, item.ID);
            if (File.Exists(path)) return false;
            return WriteFile(path, readStream);
        }

        public virtual bool DeleteImage(AlbumItemDTO item)
        {
            var path = BuildFilePath(item.AlbumID, item.ID);
            if (File.Exists(path)) return DeleteFile(path);
            return true;
        }

        public virtual bool IsMimeTypeAllowed(string mimeType)
        {
            return ALLOWED_TYPES.Contains(mimeType?.ToLower());
        }
    }
}