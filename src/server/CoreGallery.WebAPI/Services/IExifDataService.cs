﻿using System;
using System.Drawing;
using System.IO;
using CoreGallery.WebAPI.DTOs;
using ExifLib;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace CoreGallery.WebAPI.Services
{
    public interface IExifDataService
    {
        AugmentedAlbumItemDTO ReadExifDataFromFile(IFormFile formFile);
        AugmentedAlbumItemDTO ReadExifDataFromFile(Stream fileStream);
        RotateFlipType GetRotationFromExifData(Stream fileStream);
    }

    public class ExifDataService : IExifDataService
    {
        private readonly ILogger<ExifDataService> logger;

        public ExifDataService(
            ILogger<ExifDataService> logger)
        {
            this.logger = logger;
        }

        public RotateFlipType GetRotationFromExifData(Stream fileStream)
        {
            fileStream.Position = 0;

            try
            {
                using (var reader = new ExifReader(fileStream))
                {
                    if (reader.GetTagValue(ExifTags.Orientation, out ushort orientation))
                    {
                        switch (orientation)
                        {
                            case 1:
                                return RotateFlipType.RotateNoneFlipNone;
                            case 2:
                                return RotateFlipType.RotateNoneFlipX;
                            case 3:
                                return RotateFlipType.Rotate180FlipNone;
                            case 4:
                                return RotateFlipType.Rotate180FlipX;
                            case 5:
                                return RotateFlipType.Rotate90FlipX;
                            case 6:
                                return RotateFlipType.Rotate90FlipNone;
                            case 7:
                                return RotateFlipType.Rotate270FlipX;
                            case 8:
                                return RotateFlipType.Rotate270FlipNone;
                            default:
                                return RotateFlipType.RotateNoneFlipNone;
                        }
                    }
                }
            }
            catch (ExifLibException) { /* ignored */ }

            return RotateFlipType.RotateNoneFlipNone;
        }

        public AugmentedAlbumItemDTO ReadExifDataFromFile(IFormFile formFile)
        {
            var data = ReadExifDataFromFile(formFile.OpenReadStream());
            data.ItemName = formFile.FileName;
            data.ImageFormat = formFile.ContentType;
            return data;
        }

        public AugmentedAlbumItemDTO ReadExifDataFromFile(Stream fileStream)
        {
            var imageBuffer = new byte[fileStream.Length];
            fileStream.Read(imageBuffer);

            var item = new AugmentedAlbumItemDTO
            {
                FileContent = imageBuffer
            };

            try
            {
                using (var reader = new ExifReader(new MemoryStream(item.FileContent)))
                {
                    item.ImageWidth = TryReadIntValue(reader, ExifTags.PixelXDimension);
                    item.ImageHeight = TryReadIntValue(reader, ExifTags.PixelYDimension);

                    item.Author = ReadStringValue(reader, ExifTags.Artist);
                    item.CopyrightNotice = ReadStringValue(reader, ExifTags.Copyright);

                    item.TakenAt = ReadDateTimeValue(reader, ExifTags.DateTimeOriginal);

                    item.CameraMaker = ReadStringValue(reader, ExifTags.Make);
                    item.CameraModel = ReadStringValue(reader, ExifTags.Model);

                    var latitude = ReadDoubleArrayValue(reader, ExifTags.GPSLatitude);
                    var latitudeRef = ReadStringValue(reader, ExifTags.GPSLatitudeRef);
                    var longitude = ReadDoubleArrayValue(reader, ExifTags.GPSLongitude);
                    var longitudeRef = ReadStringValue(reader, ExifTags.GPSLongitudeRef);
                    var altitude = ReadDoubleValue(reader, ExifTags.GPSAltitude);
                    var altitudeRef = ReadByteValue(reader, ExifTags.GPSAltitudeRef);

                    item.PositionLatitude = ConvertCoordinate(latitude, "N" == latitudeRef?.ToUpper());
                    item.PositionLongitude = ConvertCoordinate(longitude, "E" == longitudeRef?.ToUpper());
                    item.PositionAltitude = ConvertAltitude(altitude, altitudeRef);
                }
            }
            catch (ExifLibException ex)
            {
                logger.LogWarning(ex, $"Failed to read submitted file, returning a non-filled object");
            }

            return item;
        }

        private static float? ConvertCoordinate(double[] coord, bool positiveRef)
        {
            if (coord == null || coord.Length != 3) return null;

            var val = coord[0];
            val += coord[1] / 60;
            val += coord[2] / 60 / 60;

            if (!positiveRef) val = val * -1;

            return (float?)val;
        }

        private static float? ConvertAltitude(double? altitude, byte? belowSeaLevel)
        {
            if (altitude.HasValue && belowSeaLevel.HasValue)
                return (float?)((belowSeaLevel == 1) ? -altitude.Value : altitude.Value);
            else if (altitude.HasValue)
                return (float?)altitude;
            else
                return null;
        }

        private static int? TryReadIntValue(ExifReader reader, ExifTags tag)
        {
            try
            {
                return ReadIntValue(reader, tag);
            }
            catch (InvalidCastException) { }

            try
            {
                return ReadUshortValue(reader, tag);
            }
            catch (InvalidCastException) { }

            try
            {
                return (int?)ReadUintValue(reader, tag);
            }
            catch (InvalidCastException) { }

            return null;
        }

        private static int? ReadIntValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out int val) ? val : (int?)null;
        }

        private static uint? ReadUintValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out uint val) ? val : (uint?)null;
        }

        private static ushort? ReadUshortValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out ushort val) ? val : (ushort?)null;
        }

        private static double? ReadDoubleValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out double val) ? val : (double?)null;
        }

        private static double[] ReadDoubleArrayValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out double[] val) ? val : null;
        }

        private static float? ReadFloatValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out float val) ? val : (float?)null;
        }

        private static string ReadStringValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out string val) ? val : null;
        }

        private static DateTime? ReadDateTimeValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out DateTime val) ? val : default(DateTime?);
        }

        private static byte? ReadByteValue(ExifReader reader, ExifTags tag)
        {
            return reader.GetTagValue(tag, out byte val) ? val : (byte?)null;
        }
    }
}