﻿namespace CoreGallery.WebAPI.Services
{
    public interface IAutoSetupService
    {
        bool IsSetup();
    }
}