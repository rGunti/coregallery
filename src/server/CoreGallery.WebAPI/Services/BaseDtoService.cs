﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Repositories.Base;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.Services
{
    public abstract class BaseDtoService<TRepository, TEntity, TDto>
        : IDtoService<TDto>
        where TRepository : IRepository<TEntity>
        where TEntity : IEntity
        where TDto : IDTO
    {
        protected readonly TRepository repository;
        protected readonly IMapper mapper;

        protected BaseDtoService(
            TRepository repository,
            IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public virtual IEnumerable<TDto> GetAll()
        {
            return mapper.Map<IList<TDto>>(repository.GetAll().ToList());
        }

        public virtual TDto Get(object key)
        {
            return mapper.Map<TDto>(repository.Get(key));
        }
    }

    public abstract class BaseDtoService<TRepository, TEntity, TKey, TDto>
        : BaseDtoService<TRepository, TEntity, TDto>, IDtoService<TDto, TKey>
        where TRepository : IRepository<TEntity, TKey>
        where TEntity : ISimpleEntity<TKey>
        where TDto : ISimpleDTO<TKey>
    {
        protected BaseDtoService(
            TRepository repository,
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        [Obsolete("It is preferred to use to typed Get() method")]
        public override TDto Get(object key)
        {
            return base.Get(key);
        }

        public virtual TDto Get(TKey key)
        {
            return mapper.Map<TDto>(repository.Get(key));
        }
    }

    public abstract class BaseDtoService<TRepository, TEntity, TEntityKey, TDto, TDtoKey>
        : BaseDtoService<TRepository, TEntity, TDto>, IDtoService<TDto, TDtoKey>
        where TRepository : IRepository<TEntity, TEntityKey>
        where TEntity : ISimpleEntity<TEntityKey>
        where TDto : ISimpleDTO<TDtoKey>
    {
        protected BaseDtoService(
            TRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        protected virtual TEntityKey ConvertKey(TDtoKey key)
        {
            if (key == null) return default(TEntityKey);
            if (typeof(TEntityKey) == typeof(TDtoKey))
            {
                return (TEntityKey)Convert.ChangeType(key, typeof(TEntityKey));
            }

            throw new InvalidCastException($"Could not cast key {typeof(TDtoKey)} to {typeof(TEntityKey)} because no conversion method has been defined.");
        }

        public virtual TDto Get(TDtoKey key)
        {
            return mapper.Map<TDto>(repository.Get(ConvertKey(key)));
        }
    }

    public abstract class BaseCrudDtoService<TRepository, TEntity, TEntityKey, TDto, TDtoKey>
        : BaseDtoService<TRepository, TEntity, TEntityKey, TDto, TDtoKey>, ICrudService<TDto, TDtoKey>
        where TRepository : ICrudEntityRepository<TEntity, TEntityKey>
        where TEntity : ISimpleEntity<TEntityKey>
        where TDto : ISimpleDTO<TDtoKey>
    {
        protected BaseCrudDtoService(
            TRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        public virtual TDto Create(TDto dto)
        {
            var entity = mapper.Map<TEntity>(dto);
            var newEntity = repository.Create(entity);
            return mapper.Map<TDto>(newEntity);
        }

        public virtual void Update(TDto dto)
        {
            repository.Update(mapper.Map<TEntity>(dto));
        }

        public virtual void Delete(TDto dto)
        {
            repository.Delete(mapper.Map<TEntity>(dto));
        }

        public virtual void Delete(TDtoKey key)
        {
            repository.Delete(ConvertKey(key));
        }
    }
}