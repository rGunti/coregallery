﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace CoreGallery.WebAPI.Services
{
    public interface IFileService : IService
    {
    }

    public abstract class BaseFileService : IFileService
    {
        protected ILogger logger;

        protected BaseFileService(ILogger logger)
        {
            this.logger = logger;
        }

        protected bool WriteFile(string path, byte[] content)
        {
            try
            {
                logger.LogDebug($"Writing {content.LongLength} bytes to {path} ...");
                File.WriteAllBytes(path, content);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to store file {path} with {content.LongLength} bytes!");
                return false;
            }
        }

        protected bool WriteFile(string path, Stream stream)
        {
            try
            {
                logger.LogDebug($"Reading {stream.Length} bytes from stream ...");
                byte[] contentBuffer = new byte[stream.Length];
                stream.Read(contentBuffer);
                WriteFile(path, contentBuffer);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to store stream file {path} with {stream.Length} bytes!");
                return false;
            }
        }

        protected byte[] ReadFile(string path)
        {
            try
            {
                logger.LogDebug($"Reading file from {path} ...");
                return File.ReadAllBytes(path);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to read file from {path}!");
                return null;
            }
        }

        protected Stream ReadFileAsStream(string path)
        {
            try
            {
                logger.LogDebug($"Opening file stream from {path} ...");
                return new FileInfo(path).OpenRead();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to open file stream from {path}!");
                return null;
            }
        }

        protected bool DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Failed to delete file {path}!");
                return false;
            }
        }
    }
}