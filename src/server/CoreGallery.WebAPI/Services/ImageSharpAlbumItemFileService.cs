﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using CoreGallery.WebAPI.DTOs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using Image = SixLabors.ImageSharp.Image;

namespace CoreGallery.WebAPI.Services
{
    public class ImageSharpAlbumItemFileService : AlbumItemFileService
    {
        public ImageSharpAlbumItemFileService(
            ILogger<AlbumItemFileService> logger, 
            IConfiguration configuration, 
            IAlbumService albumService, 
            IExifDataService exifDataService)
            : base(logger, configuration, albumService, exifDataService)
        {
        }

        protected override void BuildThumbnailImage(AlbumItemDTO item, int width, int height, string thumbnailPath)
        {
            logger.LogInformation($"Generating thumbnail for {item.AlbumID}/{item.ID} using ImageSharp ({Path.GetFileName(thumbnailPath)}) ...");

            if (width == -1 && height == -1)
            {
                height = 240;
            }

            using (Image<Rgba32> sourceImage = Image.Load(BuildFilePath(item.AlbumID, item.ID)))
            {
                // TODO: ImageSharp can apparently also read metadata, investigate implementation
                var originalStream = GetImage(item);
                var orientation = exifDataService.GetRotationFromExifData(originalStream);
                var isLandscape = LANDSCAPE_ORIENTATIONS.Contains(orientation);

                // Determine exact thumb width / height
                int sourceWidth = isLandscape ? sourceImage.Width : sourceImage.Height;
                int sourceHeight = isLandscape ? sourceImage.Height : sourceImage.Width;
                var thumbnailSize = CalculateThumbnailSize(
                    sourceWidth, sourceHeight,
                    width, height);
                width = thumbnailSize.Width;
                height = thumbnailSize.Height;

                // Mutate
                sourceImage.Mutate(i => i
                    .Rotate(ConvertSystemDrawingToRotateMode(orientation))
                    .Resize(width, height)
                );
                sourceImage.Save(thumbnailPath);
            }
        }

        private static RotateMode ConvertSystemDrawingToRotateMode(
            RotateFlipType rotateFlipType)
        {
            switch (rotateFlipType)
            {
                case RotateFlipType.RotateNoneFlipNone:
                    return RotateMode.None;
                case RotateFlipType.Rotate90FlipNone:
                    return RotateMode.Rotate90;
                case RotateFlipType.Rotate180FlipNone:
                    return RotateMode.Rotate180;
                case RotateFlipType.Rotate270FlipNone:
                    return RotateMode.Rotate270;

                case RotateFlipType.Rotate180FlipX:
                case RotateFlipType.Rotate180FlipY:
                case RotateFlipType.Rotate270FlipX:
                case RotateFlipType.Rotate270FlipY:
                default:
                    return RotateMode.None;
            }
        }
    }
}