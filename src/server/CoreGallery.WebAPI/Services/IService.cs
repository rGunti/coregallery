﻿using System.Collections.Generic;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.Services
{
    public interface IService
    {
    }

    public interface IDtoService<TDto>
        where TDto : IDTO
    {
        IEnumerable<TDto> GetAll();
        TDto Get(object key);
    }

    public interface IDtoService<TDto, TKey>
        where TDto : ISimpleDTO<TKey>
    {
        TDto Get(TKey key);
    }

    public interface ICrudService<TDto> : IDtoService<TDto>
        where TDto : IDTO
    {
        TDto Create(TDto dto);
        void Update(TDto dto);
        void Delete(TDto dto);
    }

    public interface ICrudService<TDto, TKey> : ICrudService<TDto>, IDtoService<TDto, TKey>
        where TDto : ISimpleDTO<TKey>
    {
        void Delete(TKey key);
    }
}