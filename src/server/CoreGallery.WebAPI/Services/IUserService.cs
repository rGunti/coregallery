﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.Exceptions;

namespace CoreGallery.WebAPI.Services
{
    public interface IUserService
        : ICrudService<UserDTO, string>, IAutoSetupService
    {
        TokenEnrichedUserDTO Authenticate(UserDTO user);
        TokenEnrichedUserDTO Authenticate(string username, string password);

        bool UserHasPermissions(string username, params PermissionKey[] permissionKey);
        bool UserHasPermissions(string username, IEnumerable<PermissionKey> permissions);

        UserDTO ReactivateUser(string username);
        UserDTO ResetPassword(string username);
    }

    public class UserService :
        BaseCrudDtoService<IUserRepository, User, string, UserDTO, string>,
        IUserService
    {
        public UserService(
            IUserRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        public bool IsSetup()
        {
            return repository.GetAll().Any();
        }

        public override IEnumerable<UserDTO> GetAll()
        {
            return mapper.Map<IList<UserDTO>>(repository.GetUsersWithRoleAndPermissions());
        }

        public override UserDTO Get(string username)
        {
            return mapper.Map<UserDTO>(repository.GetUserWithRoleAndPermissions(username));
        }

        public TokenEnrichedUserDTO Authenticate(UserDTO user)
        {
            return Authenticate(user.ID, user.Password);
        }

        public TokenEnrichedUserDTO Authenticate(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return null;

            var user = repository.GetUserWithRoleAndPermissions(username);
            if (user == null)
                return null;

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            var dto = mapper.Map<TokenEnrichedUserDTO>(user);
            dto.Role.Permissions = user.Role.RolePermissionAssignments.Select(a => a.PermissionKey).ToList();
            return dto;
        }

        public override UserDTO Create(UserDTO user)
        {
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new InputValidationException("Password", "cannot be empty");

            if (repository.Get(user.ID) != null)
                throw new InputValidationException("Username", "already exists");

            var userEntity = mapper.Map<User>(user);
            UpdatePassword(user, userEntity);

            userEntity = repository.Create(userEntity);
            return mapper.Map<UserDTO>(userEntity);
        }

        public override void Update(UserDTO user)
        {
            var userEntity = mapper.Map<User>(user);
            UpdatePassword(user, userEntity);

            repository.Update(userEntity);
        }

        public bool UserHasPermissions(string username, params PermissionKey[] permissionKeys)
        {
            return UserHasPermissions(username, permissionKeys.ToList());
        }

        public bool UserHasPermissions(string username, IEnumerable<PermissionKey> wantsPermissions)
        {
            var user = repository.GetUserWithRoleAndPermissions(username);
            if (user == null)
                throw new InvalidOperationException(nameof(username));

            var hasPermissions = user.Role.RolePermissionAssignments
                .Select(a => a.PermissionKey)
                .ToList();

            return !wantsPermissions.Except(hasPermissions).Any();
        }

        /* *** Password *** */
        private void UpdatePassword(UserDTO dto, User entity)
        {
            if (!string.IsNullOrWhiteSpace(dto.Password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(dto.Password, out passwordHash, out passwordSalt);

                entity.PasswordHash = passwordHash;
                entity.PasswordSalt = passwordSalt;
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public UserDTO ReactivateUser(string username)
        {
            var user = repository.GetUserWithRoleAndPermissions(username);
            if (user?.DeletedAt == null)
                return null;

            var newPassword = GenerateNewPassword();

            user.DeletedAt = null;
            UpdatePassword(new UserDTO() { Password = newPassword }, user);

            repository.Update(user);
            user = repository.GetUserWithRoleAndPermissions(username);

            var dto = mapper.Map<UserDTO>(user);
            dto.Password = newPassword;
            return dto;
        }

        public UserDTO ResetPassword(string username)
        {
            var user = repository.GetUserWithRoleAndPermissions(username);
            if (user == null || user.DeletedAt != null)
                return null;

            var newPassword = GenerateNewPassword();

            UpdatePassword(new UserDTO() { Password = newPassword }, user);

            repository.Update(user);
            user = repository.GetUserWithRoleAndPermissions(username);

            var dto = mapper.Map<UserDTO>(user);
            dto.Password = newPassword;
            return dto;
        }

        const string PW_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        static readonly Random PW_RANDOM = new Random(DateTime.UtcNow.Millisecond);

        private static string GenerateNewPassword(int length = 8)
        {
            return new string(Enumerable.Repeat(PW_CHARS, length)
                .Select(s => s[PW_RANDOM.Next(s.Length)]).ToArray());
        }
    }
}