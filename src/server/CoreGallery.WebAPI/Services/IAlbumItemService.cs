﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories;
using CoreGallery.Extensions.Paging;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.DTOs.Base;
using Microsoft.AspNetCore.Http;

namespace CoreGallery.WebAPI.Services
{
    public interface IAlbumItemService
        : ICrudService<AlbumItemDTO, int>
    {
        IEnumerable<AlbumItemDTO> GetItemsFromAlbum(int albumID);
        AlbumItemDTO GetItem(int albumID, int itemID);
        AlbumItemDTO Create(IFormFile formFile, string userID, int albumID);
        IPagedDtoResult<AlbumItemDTO> GetPagesItemsFromAlbum(int albumID, int page);

        INeighborhood<AlbumItemDTO> GetNeighborhoodOfAlbumItem(int albumId, int itemId);
    }

    public class AlbumItemService :
        BaseCrudDtoService<IAlbumItemRepository, AlbumItem, int, AlbumItemDTO, int>,
        IAlbumItemService
    {
        private readonly IAlbumItemFileService fileService;
        private readonly IExifDataService exifDataService;

        public AlbumItemService(
            IAlbumItemRepository repository, 
            IMapper mapper,
            IAlbumItemFileService fileService,
            IExifDataService exifDataService)
            : base(repository, mapper)
        {
            this.fileService = fileService;
            this.exifDataService = exifDataService;
        }

        public IEnumerable<AlbumItemDTO> GetItemsFromAlbum(int albumID)
        {
            return mapper.Map<IList<AlbumItemDTO>>(
                repository.GetItemsInAlbum(albumID).OrderBy(i => i.CreatedAt));
        }

        public AlbumItemDTO GetItem(int albumID, int itemID)
        {
            return mapper.Map<AlbumItemDTO>(repository.GetItemInAlbum(albumID, itemID));
        }

        public AlbumItemDTO Create(
            IFormFile formFile, 
            string userID, 
            int albumID)
        {
            var image = exifDataService.ReadExifDataFromFile(formFile);
            image.UserID = userID;
            image.AlbumID = albumID;

            var outputImage = Create(image);
            fileService.StoreImage(outputImage, image.FileContent);
            return outputImage;
        }

        public override void Delete(int key)
        {
            var item = Get(key);
            base.Delete(key);
            fileService.DeleteImage(item);
        }

        public IPagedDtoResult<AlbumItemDTO> GetPagesItemsFromAlbum(int albumID, int page)
        {
            return mapper.Map<PagedDtoResult<AlbumItemDTO>>(
                repository.GetPagedItemsInAlbum(albumID, page, 24));
        }

        public INeighborhood<AlbumItemDTO> GetNeighborhoodOfAlbumItem(int albumId, int itemId)
        {
            return mapper.Map<Neighborhood<AlbumItemDTO>>(
                repository.GetNeighborhoodOfItem(albumId, itemId));
        }
    }
}
