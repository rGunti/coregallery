﻿using System.Linq;
using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;

namespace CoreGallery.WebAPI.Services
{
    public interface IPermissionService
        : ICrudService<PermissionDTO, PermissionKey>
    {
    }

    public class PermissionService :
        BaseCrudDtoService<IPermissionRepository, Permission, PermissionKey, PermissionDTO, PermissionKey>,
        IPermissionService
    {
        public PermissionService(
            IPermissionRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}