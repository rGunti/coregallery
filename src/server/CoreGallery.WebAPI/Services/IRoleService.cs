﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Repositories;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;

namespace CoreGallery.WebAPI.Services
{
    public interface IRoleService
        : ICrudService<RoleDTO, RoleKey>
    {
    }

    public class RoleService :
        BaseCrudDtoService<IRoleRepository, Role, RoleKey, RoleDTO, RoleKey>,
        IRoleService
    {
        public RoleService(
            IRoleRepository repository, 
            IMapper mapper)
            : base(repository, mapper)
        {
        }

        public override RoleDTO Get(RoleKey key)
        {
            var role = repository.Get(key);
            var dto = mapper.Map<RoleDTO>(role);
            dto.Permissions = role.RolePermissionAssignments
                .Select(a => a.PermissionKey)
                .ToList();
            return dto;
        }

        public override IEnumerable<RoleDTO> GetAll()
        {
            var roles = repository.GetAll().ToList();
            var dtos = mapper.Map<IEnumerable<RoleDTO>>(roles).ToList();
            foreach (var dto in dtos)
            {
                dto.Permissions = roles
                    .First(r => r.ID == dto.ID)
                    .RolePermissionAssignments
                    .Select(a => a.PermissionKey)
                    .ToList();
            }

            return dtos;
        }
    }
}
