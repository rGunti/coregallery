﻿using AutoMapper;
using CoreGallery.Db;
using CoreGallery.Db.Repositories.Impl;
using CoreGallery.WebAPI.Services;
using CoreGallery.WebAPI.Utils.Authentication;
using CoreGallery.WebAPI.Utils.Authorization;
using CoreGallery.WebAPI.Utils.AutoSetup;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoreGallery.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(o =>
                {
                    o.SerializerSettings.Converters.Add(new StringEnumConverter());
                    o.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });
            services.AddCors(o => { o.AddDefaultPolicy(p => { p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); }); });
            services.AddDbContext<CoreGalleryContext>(
                o => o.UseMySQL(Configuration.GetConnectionString("default")));
            services.AddCoreGalleryRepositories();
            services.AddAutoMapper();
            services.AddCoreGalleryDtoServices();
            services.AddCoreGalleryAuthentication();
            services.AddCoreGalleryAuthorization();
            services.AddAutoSetup();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env,
            DatabaseAutoSetup autoSetup)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            autoSetup.Execute();

            app.UseCors();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
