﻿using System;

namespace CoreGallery.WebAPI.Exceptions
{
    public class CoreGalleryApiException : Exception
    {
        public CoreGalleryApiException()
        {
        }

        public CoreGalleryApiException(string message) : base(message)
        {
        }

        public CoreGalleryApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}