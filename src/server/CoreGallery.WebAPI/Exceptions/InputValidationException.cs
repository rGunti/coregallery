﻿namespace CoreGallery.WebAPI.Exceptions
{
    public class InputValidationException : CoreGalleryApiException
    {
        public InputValidationException() : base() { }
        public InputValidationException(
            string fieldName,
            string rule)
            : base($"Input validation failed for field {fieldName} because {fieldName} {rule}")
        {
            FieldName = fieldName;
            RuleDescription = rule;
        }

        public string FieldName { get; }
        public string RuleDescription { get; }
    }
}