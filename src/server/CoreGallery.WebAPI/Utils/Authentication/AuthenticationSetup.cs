﻿using System;
using System.Text;
using System.Threading.Tasks;
using CoreGallery.WebAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CoreGallery.WebAPI.Utils.Authentication
{
    public static class AuthenticationSetup
    {
        public const string CRYPTO_KEY = "SampleKey123@!#More"; // TODO: Config

        public static IServiceCollection AddCoreGalleryAuthentication(
            this IServiceCollection services,
            string key = CRYPTO_KEY)
        {
            var byteKey = Encoding.ASCII.GetBytes(key);

            services
                .AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(ConfigureJwtBearerToken(byteKey));

            return services;
        }

        private static Action<JwtBearerOptions> ConfigureJwtBearerToken(byte[] key)
        {
            return o =>
            {
                o.Events = new JwtBearerEvents
                {
                    OnTokenValidated = ValidateToken,
                    OnMessageReceived = MessageReceived
                };
                o.RequireHttpsMetadata = false;
                o.SaveToken = true;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            };
        }

        private static Task ValidateToken(TokenValidatedContext context)
        {
            var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
            var userName = context.Principal.Identity.Name;
            var user = userService.Get(userName);
            if (user == null)
                context.Fail("Unauthorized");

            return Task.CompletedTask;
        }

        private static Task MessageReceived(MessageReceivedContext context)
        {
            if (string.IsNullOrWhiteSpace(context.Token)
                && context.Request.Query.ContainsKey("token"))
            {
                context.Token = context.Request.Query["token"];
            }
            return Task.CompletedTask;
        }
    }
}