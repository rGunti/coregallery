﻿using System;
using System.Collections.Generic;
using CoreGallery.Extensions.DbExtensions.Description;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CoreGallery.WebAPI.Utils.AutoSetup
{
    public static class AutoSetupExtension
    {
        public static IServiceCollection AddAutoSetup(this IServiceCollection services)
        {
            return services
                .AddScoped<DatabaseAutoSetup>();
        }
    }

    public interface IAutoSetup
    {
        void Execute();
    }

    public abstract class AutoSetup<TSelf> : IAutoSetup
        where TSelf : AutoSetup<TSelf>
    {
        protected readonly ILogger<TSelf> logger;

        protected AutoSetup(
            ILogger<TSelf> logger)
        {
            this.logger = logger;
        }

        public void Execute()
        {
            logger.LogInformation($"Running {GetType().Name} ...");
            Main();
        }

        protected abstract void Main();
    }

    public class DatabaseAutoSetup : AutoSetup<DatabaseAutoSetup>
    {
        private readonly IUserService userService;
        private readonly IRoleService roleService;

        public DatabaseAutoSetup(
            ILogger<DatabaseAutoSetup> logger,
            IUserService userService,
            IRoleService roleService)
            : base(logger)
        {
            this.userService = userService;
            this.roleService = roleService;
        }

        protected override void Main()
        {
            var runUserSetup = !userService.IsSetup();
            if (runUserSetup)
                RunUserSetup();
        }

        private void RunUserSetup()
        {
            logger.LogInformation("No registered user available, creating default user ...");
            userService.Create(new UserDTO
            {
                ID = "admin",
                Email = "admin@example.com",
                Password = "toor",
                Role = roleService.Get(RoleKey.SuperUser)
            });
        }
    }
}