﻿using AutoMapper;
using CoreGallery.Db.Entities;
using CoreGallery.Db.Entities.Base;
using CoreGallery.Db.Repositories.Utils;
using CoreGallery.Extensions.Paging;
using CoreGallery.WebAPI.DTOs;
using CoreGallery.WebAPI.DTOs.Base;

namespace CoreGallery.WebAPI.Utils.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateEntityToDtoMapping<Permission, PermissionDTO>();
            CreateEntityToDtoMapping<Role, RoleDTO>();
            CreateEntityToDtoMapping<User, UserDTO>();
            CreateEntityToDtoMapping<User, TokenEnrichedUserDTO>();

            CreateEntityToDtoMapping<Album, AlbumDTO>();
            CreateEntityToDtoMapping<AlbumItem, AlbumItemDTO>();
            CreateEntityToDtoMapping<AlbumItem, AugmentedAlbumItemDTO>();

            CreateMap<PagedQueryResult<IEntity>, PagedDtoResult<IDTO>>();
            CreateMap<Neighborhood<IEntity>, Neighborhood<IDTO>>();
        }

        private void CreateEntityToDtoMapping<TEntity, TDto>()
            where TEntity : IEntity
            where TDto : IDTO
        {
            CreateMap<TEntity, TDto>();
            CreateMap<TDto, TEntity>();
        }
    }
}
