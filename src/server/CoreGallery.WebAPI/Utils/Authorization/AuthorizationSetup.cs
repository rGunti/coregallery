﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoreGallery.Extensions.Permissions;
using CoreGallery.WebAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace CoreGallery.WebAPI.Utils.Authorization
{
    public static class AuthorizationSetup
    {
        public const string POLICY_NAME = "CoreGalleryPermissionPolicy";

        public static IServiceCollection AddCoreGalleryAuthorization(
            this IServiceCollection services)
        {
            return services
                .AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>()
                .AddAuthorization(o =>
                {
                    o.AddPolicy(POLICY_NAME, p =>
                    {
                        p.AddRequirements(new PermissionAuthorizationRequirement());
                    });
                });
        }
    }

    public class PermissionAuthorizationRequirement : IAuthorizationRequirement
    {
        public PermissionKey RequiredPermission { get; }

        public PermissionAuthorizationRequirement() { }
        public PermissionAuthorizationRequirement(PermissionKey requiredPermission)
        {
            RequiredPermission = requiredPermission;
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionAttribute : AuthorizeAttribute
    {
        public PermissionKey Permission { get; }

        public PermissionAttribute(PermissionKey permission) : base(AuthorizationSetup.POLICY_NAME)
        {
            Permission = permission;
        }
    }

    public abstract class AttributeAuthorizationHandler<TRequirement, TAttribute> : AuthorizationHandler<TRequirement> where TRequirement : IAuthorizationRequirement where TAttribute : Attribute
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement)
        {
            var attributes = new List<TAttribute>();

            var action = (context.Resource as AuthorizationFilterContext)?.ActionDescriptor as ControllerActionDescriptor;
            if (action != null)
            {
                attributes.AddRange(GetAttributes(action.ControllerTypeInfo.UnderlyingSystemType));
                attributes.AddRange(GetAttributes(action.MethodInfo));
            }

            return HandleRequirementAsync(context, requirement, attributes);
        }

        protected abstract Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement, IEnumerable<TAttribute> attributes);

        private static IEnumerable<TAttribute> GetAttributes(MemberInfo memberInfo)
        {
            return memberInfo.GetCustomAttributes(typeof(TAttribute), false).Cast<TAttribute>();
        }
    }


    public class PermissionAuthorizationHandler : AttributeAuthorizationHandler<PermissionAuthorizationRequirement, PermissionAttribute>
    {
        private readonly IUserService userService;

        public PermissionAuthorizationHandler(IUserService userService)
        {
            this.userService = userService;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizationRequirement requirement, IEnumerable<PermissionAttribute> attributes)
        {
            if (context.User == null)
            {
                context.Fail();
                return;
            }

            if (!userService.UserHasPermissions(context.User.Identity.Name, attributes.Select(a => a.Permission).ToArray()))
            {
                return;
            }

            context.Succeed(requirement);
        }
    }

}