﻿namespace CoreGallery.WebAPI.Responses
{
    public interface IResponse
    {
        bool Ok { get; set; }
    }

    public interface IErrorResponse<T>
    {
        T Error { get; set; }
    }

    public class SimpleResponse : IResponse
    {
        public SimpleResponse(bool ok = true)
        {
            Ok = ok;
        }

        public bool Ok { get; set; }
    }

    public class SimpleErrorResponse : SimpleResponse,  IErrorResponse<string>
    {
        public SimpleErrorResponse() : base(false) { }
        public SimpleErrorResponse(string error) : base(false)
        {
            Error = error;
        }

        public string Error { get; set; }
    }
}