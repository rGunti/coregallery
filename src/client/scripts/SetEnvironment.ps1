param(
    [string]$ApiServer
)

(Get-Content -Path .\src\environments\TEMPLATE.environment.prod.ts) | ForEach-Object {$_ -Replace '#_API_SERVER_#', $ApiServer} | Set-Content -Path .\src\environments\environment.prod.ts
