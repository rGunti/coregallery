export interface IDto { }

export interface ISimpleDto<TKey> extends IDto {
    id: TKey;
}

export interface ITrackedDto {
    createdAt?: Date;
    updatedAt?: Date;
}

export interface INonDestructableDto {
    deletedAt?: Date;
}

export interface IPagedResult<TEntity> {
    items: TEntity[];
    totalItems: number;
}

export interface INeighborhood<TEntity> {
    current: TEntity;
    next?: TEntity;
    previous?: TEntity;
}
