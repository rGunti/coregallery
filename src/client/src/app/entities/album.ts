import { ISimpleDto, ITrackedDto } from './base';

export interface AlbumDto extends ISimpleDto<number>, ITrackedDto {
    name: string;
    userID: string;
    isPrivateAlbum: boolean;
}

export interface AlbumItemDto extends ISimpleDto<number>, ITrackedDto {
    albumID: number;
    userID: string;
    itemName: string;

    imageFormat: string;
    imageWidth?: number;
    imageHeight?: number;
    bitDepth?: number;

    author: string;
    copyrightNotice: string;

    takenAt?: Date;

    cameraMaker?: string;
    cameraModel?: string;

    positionDescription?: string;
    positionLatituide?: number;
    positionLongitude?: number;
    positionAltitude?: number;
}

export interface AugmentedAlbumItemDto extends AlbumItemDto {
    fileContent: string|any;
}

export function constructAlbumItemDto(albumId: number, itemId: number): AlbumItemDto {
    return {
        id: itemId,
        albumID: albumId,
        userID: null,
        itemName: null,
        imageFormat: null,
        author: null,
        copyrightNotice: null
    };
}
