import { ISimpleDto, ITrackedDto, INonDestructableDto } from './base';
import { PermissionKey } from '../utils/auth/permissions';

export interface UserDto extends ISimpleDto<string>, ITrackedDto, INonDestructableDto {
    password?: string;
    email: string;
    role: RoleDto;
}

export interface TokenEnrichedUserDto extends UserDto {
    token: string;
}

export interface RoleDto extends ISimpleDto<string> {
    description: string;
    permissions: string[];
}

export interface PermissionDto extends ISimpleDto<string> {
    description: string;
}

export interface SimpleLoginRequestBody {
    username: string;
    password: string;
}

export interface JwtBody {
    email: string;
    permissions: string;
    role: string;
    unique_name: string;
}
