import { Component } from '@angular/core';
import { AuthenticationService } from './utils/authentication.service';
import { PermissionKey } from './utils/auth/permissions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'coregallery-client';

  constructor(
    public authService: AuthenticationService
  ) {
  }

  get listUserPerm() { return PermissionKey.ListUser; }
  get listRolePerm() { return PermissionKey.ListUser; }

  get isLoggedIn() {
    return this.authService.isLoggedIn;
  }

  get userDetail() {
    return this.authService.userInfo;
  }
}
