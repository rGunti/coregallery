import { NgModule, Type } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { TosComponent } from './page/tos/tos.component';
import { LoginComponent } from './page/login/login.component';
import { LogoutComponent } from './page/logout/logout.component';
import { PageNotFoundComponent } from './page/error/page-not-found/page-not-found.component';
import { AuthGuard } from './utils/auth/auth.guard';
import { PermissionGuard } from './utils/auth/permission.guard';
import { PermissionKey } from './utils/auth/permissions';
import { AlbumComponent } from './page/album/album.component';
import { AlbumItemsComponent } from './page/album-items/album-items.component';
import { UsersComponent } from './page/admin/users/users.component';
import { RolesComponent } from './page/admin/roles/roles.component';

/*
export function redirectRoute(from: string, to: string): Route {
  return { path: from, redirectTo: to, pathMatch: 'full' };
}

export function simpleRoute(path: string, component: Type<any>): Route {
  return { path, component };
}

export function basicGuardedRoute(path: string, component: Type<any>, ...canActivate: any[]): Route {
  return { path, component, canActivate };
}

export function permissionGuardedRoute(path: string, component: Type<any>, ...permissions: PermissionKey[]): Route {
  return {
    path,
    component,
    canActivate: [AuthGuard, PermissionGuard],
    data: { permissions: permissions.map(i => `${i}`) }
  };
}

export function pageNotFound(component: Type<any>): Route {
  return { path: '**', component };
}

export const routes: Routes = [
  redirectRoute('', 'home'),
  simpleRoute('home', HomeComponent),
  simpleRoute('tos', TosComponent),
  simpleRoute('login', LoginComponent),
  simpleRoute('logout', LogoutComponent),
  basicGuardedRoute('test-auth', TosComponent, AuthGuard),
  permissionGuardedRoute('test-auth-perm', TosComponent,
    PermissionKey.UploadImage),
  permissionGuardedRoute('album', AlbumComponent,
    PermissionKey.ListAlbum),
  permissionGuardedRoute('album/:id', AlbumItemsComponent,
    PermissionKey.ListAlbum),
  permissionGuardedRoute('album/:id/:page', AlbumItemsComponent,
    PermissionKey.ListAlbum),
  permissionGuardedRoute('admin/users', UsersComponent,
    PermissionKey.ListUser),
  permissionGuardedRoute('admin/roles', RolesComponent,
    PermissionKey.ListUser),
  pageNotFound(PageNotFoundComponent)
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DefuncedAppRoutingModule { }
*/

@NgModule({
  imports: [RouterModule.forRoot([
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'logout', component: LogoutComponent },
    { path: 'album', component: AlbumComponent, canActivate: [AuthGuard, PermissionGuard],
      data: { permissions: [PermissionKey.ListAlbum].map(i => i.toString()) }
    },
    { path: 'album/:id', component: AlbumItemsComponent, canActivate: [AuthGuard, PermissionGuard],
      data: { permissions: [PermissionKey.ListAlbum].map(i => i.toString()) }
    },
    { path: 'album/:id/:page', component: AlbumItemsComponent, canActivate: [AuthGuard, PermissionGuard],
      data: { permissions: [PermissionKey.ListAlbum].map(i => i.toString()) }
    },
    { path: 'admin/users', component: UsersComponent, canActivate: [AuthGuard, PermissionGuard],
      data: { permissions: [PermissionKey.ListUser].map(i => i.toString()) }
    },
    { path: 'admin/roles', component: RolesComponent, canActivate: [AuthGuard, PermissionGuard],
      data: { permissions: [PermissionKey.ListUser].map(i => i.toString()) }
    },
    { path: '**', component: PageNotFoundComponent }
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
