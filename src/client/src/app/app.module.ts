import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { getStoredToken } from './utils/auth';
import { environment } from 'src/environments/environment';
import { NavbarComponent } from './base/nav/navbar/navbar.component';
import { NavbarLinkComponent } from './base/nav/navbar-link/navbar-link.component';
import { NavbarTextComponent } from './base/nav/navbar-text/navbar-text.component';
import { HomeComponent } from './page/home/home.component';
import { TosComponent } from './page/tos/tos.component';
import { NavbarDropdownComponent } from './base/nav/navbar-dropdown/navbar-dropdown.component';
import { NavbarDropdownItemComponent } from './base/nav/navbar-dropdown-item/navbar-dropdown-item.component';
import { LoginComponent } from './page/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LogoutComponent } from './page/logout/logout.component';
import { PageNotFoundComponent } from './page/error/page-not-found/page-not-found.component';
import { NavbarDropdownDividerComponent } from './base/nav/navbar-dropdown-divider/navbar-dropdown-divider.component';
import { AlbumComponent } from './page/album/album.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AlbumItemsComponent } from './page/album-items/album-items.component';
import { UsersComponent } from './page/admin/users/users.component';
import { UserModalComponent } from './page/admin/user-modal/user-modal.component';
import { RolesComponent } from './page/admin/roles/roles.component';
import { ImageModalComponent } from './page/image-modal/image-modal.component';

const baseUrlWithoutProtocol = environment.services.coreGalleryServer.replace('http://', '').replace('https://', '');

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NavbarLinkComponent,
    NavbarTextComponent,
    NavbarDropdownComponent,
    HomeComponent,
    TosComponent,
    NavbarDropdownItemComponent,
    LoginComponent,
    LogoutComponent,
    PageNotFoundComponent,
    NavbarDropdownDividerComponent,
    AlbumComponent,
    AlbumItemsComponent,
    UsersComponent,
    UserModalComponent,
    RolesComponent,
    ImageModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: getStoredToken,
        whitelistedDomains: [ baseUrlWithoutProtocol ],
        blacklistedRoutes: [ `${baseUrlWithoutProtocol}/api/user/authenticate` ]
      }
    }),
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [
    UserModalComponent,
    ImageModalComponent
  ]
})
export class CoreGalleryClientModule { }
