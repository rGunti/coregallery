import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavbarDropdownDividerComponent } from './navbar-dropdown-divider.component';

describe('NavbarDropdownDeviderComponent', () => {
  let component: NavbarDropdownDividerComponent;
  let fixture: ComponentFixture<NavbarDropdownDividerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarDropdownDividerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarDropdownDividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
