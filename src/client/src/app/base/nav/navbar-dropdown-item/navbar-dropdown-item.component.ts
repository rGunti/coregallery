import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navbar-dropdown-item',
  templateUrl: './navbar-dropdown-item.component.html',
  styleUrls: ['./navbar-dropdown-item.component.css']
})
export class NavbarDropdownItemComponent implements OnInit {

  @Input() link: string;
  @Input() disabled: boolean;

  constructor() { }

  ngOnInit() {
  }

}
