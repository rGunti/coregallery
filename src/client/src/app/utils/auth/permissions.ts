export enum PermissionKey {
    Login = 'Login',

    ListUser = 'ListUser',
    ModifyUser = 'ModifyUser',

    ListAlbum = 'ListAlbum',
    ListPrivateAlbum = 'ListPrivateAlbum',
    ModifyAlbum = 'ModifyAlbum',
    ModifyForeignAlbum = 'ModifyForeignAlbum',

    UploadImage = 'UploadImage',

    TriggerMassThumbnailGeneration = 'TriggerMassThumbnailGeneration'
}
