/* tslint:disable: no-console */
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.debug(route, state);
    return this.checkPermissions(state.url, route.data.permissions);
  }

  redirectToAccessDenied(errorUrl: string) {
    this.router.navigate(
      ['/access-denied'],
      {
        queryParams: {
          errorUrl
        }
      }
    );
  }

  redirectToLogin(redirectUrl: string) {
    this.router.navigate(
      ['/login'],
      { queryParams: { redirectUrl } }
    );
  }

  checkPermissions(url: string, permissions: string[]): boolean {
    console.debug(url, 'Is logged in:', this.authService.isLoggedIn, 'Requested permissions:', permissions);
    if (this.authService.isLoggedIn && permissions) {
      const availablePermissions = this.authService.userInfo.permissions.split(',');
      console.debug(url, 'Requested permissions:', permissions, 'Available permissions:', availablePermissions);
      for (const perm of permissions) {
        if (availablePermissions.indexOf(perm) < 0) {
          this.redirectToAccessDenied(url);
          return false;
        }
      }
      return true;
    } else if (!this.authService.isLoggedIn) {
      this.redirectToLogin(url);
    } else {
      this.redirectToAccessDenied(url);
    }
    return false;
  }

}
