
const STORAGE_KEY_TOKEN = 'coregallery_jwttoken';

export function getStoredToken() {
    return localStorage.getItem(STORAGE_KEY_TOKEN);
}

export function setStoredToken(token: string) {
    localStorage.setItem(STORAGE_KEY_TOKEN, token);
}

export function clearLocalStorage() {
    localStorage.clear();
}
