import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api-service';
import { TokenEnrichedUserDto, SimpleLoginRequestBody } from '../entities/auth';
import { setStoredToken, getStoredToken, clearLocalStorage } from './auth';
import { JwtBody } from './../entities/auth';
import { PermissionKey } from './auth/permissions';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends ApiService {

  constructor(
    private jwtHelper: JwtHelperService,
    http: HttpClient
  ) {
    super(http);
  }

  get token(): string {
    return getStoredToken();
  }

  get isLoggedIn(): boolean {
    return this.token && !this.jwtHelper.isTokenExpired();
  }

  get userInfo(): JwtBody {
    return this.jwtHelper.decodeToken(/* token (not required) */);
  }

  get permissions(): string[] {
    return this.userInfo.permissions.split(',');
  }

  async login(body: SimpleLoginRequestBody): Promise<boolean> {
    try {
      const response = await this.post<SimpleLoginRequestBody, TokenEnrichedUserDto>
        ('/api/user/authenticate', body);

      if (response) {
        setStoredToken(response.token);
        return true;
      }
    } catch (err) {
      console.error('Error while logging in: ', err);
    }
    return false;
  }

  logout() {
    clearLocalStorage();
  }

  hasPermission(permission: PermissionKey) {
    return this.permissions.indexOf(`${permission}`) >= 0;
  }

  hasPermissions(...permissions: PermissionKey[]) {
    const existingPerms = this.permissions;
    for (const perm in permissions) {
      if (existingPerms.indexOf(perm) < 0) {
        return false;
      }
    }
    return true;
  }
}
