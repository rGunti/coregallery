import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export abstract class ApiService {
    protected baseUrl: string;

    constructor(
        protected http: HttpClient
    ) {
        this.baseUrl = environment.services.coreGalleryServer;
    }

    protected getUrl(route: string): string {
        const slash = this.baseUrl.endsWith('/') || route.startsWith('/') ? '' : '/';
        return `${this.baseUrl}${slash}${route}`;
    }

    protected get<T>(route: string): Promise<T> {
        return this.http.get<T>(this.getUrl(route))
            .toPromise();
    }

    protected getBlob(route: string): Promise<Blob> {
        return this.http
            .get(this.getUrl(route), { responseType: 'blob' })
            .toPromise();
    }

    protected post<TIn, TOut>(route: string, body: TIn): Promise<TOut> {
        return this.http.post<TOut>(this.getUrl(route), body)
            .toPromise();
    }

    protected put<TIn, TOut>(route: string, body: TIn): Promise<TOut> {
        return this.http.put<TOut>(this.getUrl(route), body)
            .toPromise();
    }

    protected delete<T>(route: string): Promise<T> {
        return this.http.delete<T>(this.getUrl(route))
            .toPromise();
    }
}
