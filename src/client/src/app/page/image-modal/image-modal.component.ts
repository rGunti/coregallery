import { Component, OnInit, ViewEncapsulation, OnDestroy, HostListener } from '@angular/core';
import { NgbModalConfig, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AlbumItemDto } from 'src/app/entities/album';
import { AlbumItemService } from 'src/app/data/album-item.service';
import { INeighborhood } from 'src/app/entities/base';
import { AlbumService } from 'src/app/data/album.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.component.html',
  styleUrls: ['./image-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ImageModalComponent implements OnInit, OnDestroy {

  neighborhood: INeighborhood<AlbumItemDto>;
  imageUrl: string = null;

  constructor(
    private activeModal: NgbActiveModal,
    private modalConfig: NgbModalConfig,
    private albumItemService: AlbumItemService,
    private domSanitizer: DomSanitizer
  ) {
    this.modalConfig.backdrop = 'static';
  }

  get hasImage(): boolean {
    return !!this.neighborhood && !!this.neighborhood.current;
  }

  get safeImageUrl() {
    return this.hasImage && !!this.imageUrl ?
      this.domSanitizer.bypassSecurityTrustUrl(this.imageUrl) :
      null;
  }

  get image(): AlbumItemDto {
    return this.neighborhood ? this.neighborhood.current : null;
  }

  get hasNext(): boolean {
    return !!this.neighborhood && !!this.neighborhood.next;
  }

  get hasPrevious(): boolean {
    return !!this.neighborhood && !!this.neighborhood.previous;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.imageUrl) {
      URL.revokeObjectURL(this.imageUrl);
      this.imageUrl = null;
    }
    this.neighborhood = null;
  }

  init(image: AlbumItemDto) {
    this.ngOnDestroy();

    this.albumItemService.getNeighborhood(image)
      .then(n => {
        this.neighborhood = n;
        this.albumItemService.getFullResImageUrl(this.image)
          .then(u => {
            this.imageUrl = u;
          });
      });
  }

  onCloseClick() {
    this.activeModal.dismiss();
  }

  @HostListener('window:keyup', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowLeft':
        this.onPreviousClick();
        break;
      case 'ArrowRight':
          this.onNextClick();
          break;
    }
  }

  onPreviousClick() {
    if (this.hasImage && this.hasPrevious) {
      this.init(this.neighborhood.previous);
    }
  }

  onNextClick() {
    if (this.hasImage && this.hasNext) {
      this.init(this.neighborhood.next);
    }
  }
}
