import { Component, OnInit } from '@angular/core';
import { RolePermService } from 'src/app/data/role-perm.service';
import { RoleDto, PermissionDto } from 'src/app/entities/auth';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles: RoleDto[];
  permissions: PermissionDto[];

  constructor(
    private rolePermService: RolePermService
  ) { }

  async ngOnInit() {
    this.roles = await this.rolePermService.getRoles();
    this.permissions = await this.rolePermService.getPermissions();
  }

  roleHasPermission(role: RoleDto, permission: PermissionDto) {
    return role.permissions.indexOf(permission.id) >= 0;
  }

}
