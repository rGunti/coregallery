import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserDto, RoleDto } from 'src/app/entities/auth';
import { UserService } from 'src/app/data/user.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/utils/authentication.service';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent {

  inEditMode = false;
  user: UserDto;

  preparing = true;

  userForm = new FormGroup({
    username: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl(''),
    passwordConfirm: new FormControl(''),
    role: new FormControl('', [
      Validators.required
    ])
  });
  roleTip: string = 'Select a role to show its description';

  roles: RoleDto[];

  constructor(
    public activeModal: NgbActiveModal,
    private modalConfig: NgbModalConfig,
    private userService: UserService,
    private authService: AuthenticationService
  ) {
    this.modalConfig.backdrop = 'static';

    userService.getRoles()
      .then((roles) => {
        this.roles = roles;
      });
  }

  setUser(user: UserDto) {
    this.user = user;

    this.userForm.get('username').setValue(user.id);
    this.userForm.get('email').setValue(user.email);
    this.userForm.get('role').setValue(user.role ? user.role.id : '');

    if (user.role) {
      this.roleTip = user.role.description;
    }

    if (!this.canEditRole) {
      this.userForm.get('role').disable();
    }
  }

  get canEditRole() {
    return !this.user || this.user.id !== this.authService.userInfo.unique_name;
  }

  prepare(user: UserDto = null) {
    this.preparing = true;
    this.inEditMode = user != null;
    if (user) {
      this.userService.getUser(user.id)
        .then(u => {
          this.setUser(u);
          this.preparing = false;
        });
    } else {
      this.preparing = false;
    }
  }

  onRoleChange() {
    const role = this.getSelectedRole();
    if (role) {
      this.roleTip = role.description;
    }
  }

  getSelectedRole(): RoleDto {
    const selectedRoleId = this.userForm.get('role').value;
    return this.roles.find(r => r.id === selectedRoleId);
  }

  validate(): boolean {
    if (!this.inEditMode) {
      return this.userForm.valid
        && !!this.userForm.get('password').value
        && this.userForm.get('password').value === this.userForm.get('passwordConfirm').value;
    }
    return this.userForm.valid;
  }

  onSubmit() {
    if (this.userForm.valid) {
      const user = this.getObject();
      this.preparing = true;
      if (this.inEditMode) {
        this.userService.updateUser(user)
          .then(result => {
            if (result) {
              this.activeModal.close('commit');
            }
            this.preparing = false;
          });
      } else {
        this.userService.createUser(user)
          .then(result => {
            if (result) {
              this.activeModal.close('commit');
            }
            this.preparing = false;
          });
      }
    }
  }

  getObject(): UserDto {
    return {
      id: this.userForm.get('username').value,
      role: this.getSelectedRole(),
      email: this.userForm.get('email').value,
      password: this.inEditMode ? null : this.userForm.get('password').value
    };
  }

}
