import { Component, OnInit } from '@angular/core';
import { UserDto } from 'src/app/entities/auth';
import { UserService } from 'src/app/data/user.service';
import { AuthenticationService } from 'src/app/utils/authentication.service';
import { PermissionGuard } from 'src/app/utils/auth/permission.guard';
import { PermissionKey } from 'src/app/utils/auth/permissions';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserModalComponent } from '../user-modal/user-modal.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: UserDto[];

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) { }

  async ngOnInit() {
    this.users = await this.userService.getAllUsers();
  }

  openUserModal(user?: UserDto) {
    const modalRef = this.modalService.open(UserModalComponent, {
      centered: true
    });
    modalRef.result.then((result) => {
      if (result === 'commit') {
        this.ngOnInit();
      }
    });
    (modalRef.componentInstance as UserModalComponent).prepare(user);
  }

  canCreateUser(): boolean {
    return this.authService.hasPermission(PermissionKey.ModifyUser);
  }

  onCreateUser() {
    if (!this.canCreateUser()) { return; }
    this.openUserModal(null);
  }

  canEditUser(user: UserDto): boolean {
    return this.authService.hasPermission(PermissionKey.ModifyUser)
      && !user.deletedAt;
  }

  onEditUser(user: UserDto) {
    if (!this.canEditUser(user)) { return; }
    this.openUserModal(user);
  }

  canResetPassword(user: UserDto): boolean {
    return this.authService.hasPermission(PermissionKey.ModifyUser)
      && !user.deletedAt;
  }

  onResetPassword(user: UserDto) {
    if (!this.canResetPassword(user)) { return; }

    if (confirm(`Would you like to reset the password of "${user.id}"?`)) {
      this.userService.resetPasswordFor(user)
        .then(u => {
          if (u) {
            prompt(`User password of "${user.id}" has been reset! Find below the user password.\n`
              + 'Please write the password down, it will not be shown again.',
              u.password);
          } else {
            alert('Failed to reset user password!');
          }
          this.ngOnInit();
        });
    }
  }

  canDeleteUser(user: UserDto): boolean {
    return this.authService.hasPermission(PermissionKey.ModifyUser)
      && this.authService.userInfo.unique_name !== user.id
      && !user.deletedAt;
  }

  onDeleteUser(user: UserDto) {
    if (!this.canDeleteUser(user)) { return; }

    if (confirm(`Would you like to delete the user "${user.id}"?`)) {
      this.userService.deleteUser(user)
        .then(r => {
          if (!r) {
            alert('Failed to delete user.');
          }
          this.ngOnInit();
        });
    }
  }

  canReactivateUser(user: UserDto): boolean {
    return this.authService.hasPermission(PermissionKey.ModifyUser)
      && this.authService.userInfo.unique_name !== user.id
      && !!user.deletedAt;
  }

  onReactivateUser(user: UserDto) {
    if (!this.canReactivateUser(user)) { return; }

    if (confirm(`Would you like to reactivate the user "${user.id}"?`)) {
      this.userService.reactivateUser(user)
        .then(u => {
          if (u) {
            prompt(`User "${user.id}" has been reactivated! Find below the user password.\n`
              + 'Please write the password down, it will not be shown again.',
              u.password);
          } else {
            alert('Failed to reactivate user!');
          }
          this.ngOnInit();
        });
    }
  }

}
