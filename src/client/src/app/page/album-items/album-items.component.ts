import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlbumDto, AlbumItemDto, constructAlbumItemDto } from 'src/app/entities/album';
import { ActivatedRoute, Router, ParamMap, Event } from '@angular/router';
import { AlbumService } from 'src/app/data/album.service';
import { switchMap } from 'rxjs/operators';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { NgbDropdownConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlbumItemService } from 'src/app/data/album-item.service';
import { IPagedResult } from 'src/app/entities/base';
import { AuthenticationService } from 'src/app/utils/authentication.service';
import { PermissionKey } from 'src/app/utils/auth/permissions';
import { ImageModalComponent } from '../image-modal/image-modal.component';

@Component({
  selector: 'app-album-items',
  templateUrl: './album-items.component.html',
  styleUrls: ['./album-items.component.css']
})
export class AlbumItemsComponent implements OnInit, OnDestroy {

  static readonly PAGE_SIZE = 24;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private albumService: AlbumService,
    private albumItemService: AlbumItemService,
    private domSanitizer: DomSanitizer,
    private config: NgbDropdownConfig,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) {
    config.placement = 'bottom-right';
  }

  get totalPages() {
    return this.currentPage ? Math.ceil(this.currentPage.totalItems / AlbumItemsComponent.PAGE_SIZE) : 0;
  }

  get isForeignAlbum() {
    if (!this.album) { return false; }
    return this.album.userID !== this.authService.userInfo.unique_name;
  }

  get canNotShowToolbar() {
    return !this.canUploadImages && !this.canShowAlbumContextMenu;
  }

  get canUploadImages() {
    if (this.isForeignAlbum) {
      return this.authService.hasPermissions(
        PermissionKey.ModifyForeignAlbum,
        PermissionKey.UploadImage);
    } else {
      return this.authService.hasPermission(PermissionKey.UploadImage);
    }
  }

  get canShowAlbumContextMenu() {
    return this.canDeleteAlbum;
  }

  get canDeleteAlbum() {
    if (this.isForeignAlbum) {
      return this.authService.hasPermissions(
        PermissionKey.ModifyForeignAlbum,
        PermissionKey.UploadImage);
    } else {
      return this.authService.hasPermission(PermissionKey.ModifyAlbum);
    }
  }

  get canDeleteItem() {
    if (this.isForeignAlbum) {
      return this.authService.hasPermissions(
        PermissionKey.ModifyAlbum,
        PermissionKey.ModifyForeignAlbum);
    } else {
      return this.authService.hasPermission(PermissionKey.ModifyAlbum);
    }
  }

  albumId: number;
  album: AlbumDto;

  currentPage: IPagedResult<AlbumItemDto>;
  currentPageIndex: number = 0;

  thumbnails: Map<number, SafeUrl> = new Map<number, SafeUrl>();

  isUploadDisabled: boolean = false;
  isPagingDisabled: boolean = false;
  uploadStatus: string = 'Just a second ...';

  disposed = false;

  private hoveringImage: AlbumItemDto = null;

  async ngOnInit() {
    this.isUploadDisabled = true;
    this.isPagingDisabled = true;

    this.albumId = Number.parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.currentPageIndex = this.route.snapshot.fragment ?
      Number.parseInt(this.route.snapshot.fragment, 10) :
      0;

    this.album = await this.albumService.getAlbum(this.albumId);
    await this.loadPage();
  }

  ngOnDestroy() {
    this.disposed = true;
    this.thumbnails.forEach((v, k, m) => {
      URL.revokeObjectURL(v as string);
    });
  }

  async loadPage() {
    this.isUploadDisabled = true;
    this.isPagingDisabled = true;

    this.currentPage = await this.albumService.getAlbumItems(this.albumId, this.currentPageIndex);
    this.loadThumbnails();

    this.isUploadDisabled = false;
    this.isPagingDisabled = false;
  }

  onPageNavClick(newPage: number) {
    this.currentPageIndex = Math.min(newPage, this.totalPages - 1);
    this.loadPage();
  }

  async loadThumbnails() {
    for (const item of this.currentPage.items.filter((i) => !this.thumbnails.has(i.id))) {
      if (this.disposed) { break; }
      try {
        const thumbnail = await this.getThumbnail(item);
        this.thumbnails.set(item.id, thumbnail);
      } catch (ex) {
        console.error(ex);
      }
    }
  }

  async getThumbnail(albumItem: AlbumItemDto) {
    const thumbnail = await this.albumService.getAlbumItemThumbnail(albumItem);
    return this.domSanitizer.bypassSecurityTrustUrl(thumbnail);
  }

  getCachedThumbnail(albumItem: AlbumItemDto) {
    return this.thumbnails.get(albumItem.id);
  }

  hasCachedThumbnail(albumItem: AlbumItemDto) {
    return this.thumbnails.has(albumItem.id);
  }

  async onUploadClick(files: FileList) {
    if (files && files.length > 0) {
      this.uploadStatus = `Preparing upload of ${files.length} images ...`;
      this.isUploadDisabled = true;

      let failedFiles = [];
      for (let i = 0; i < files.length; i++) {
        const file = files.item(i);
        try {
          this.uploadStatus = `Uploading ${i+1} / ${files.length} images (${file.name}) ...`;
          await this.albumItemService.uploadFile(this.album, file);
        } catch (err) {
          console.error(file, err);
          failedFiles.push(file);
        }
      }

      this.uploadStatus = 'Just a second ...';

      if (failedFiles.length > 0) {
        const failedFilesList = failedFiles.map(f => f.name).join('\n');
        alert(`Upload of ${failedFiles.length} / ${files.length} images failed.\n${failedFilesList}`);
      }

      await this.ngOnInit();
    }
  }

  onDeleteAlbumClick() {
    if (confirm(`Would you like to delete the album "${this.album.name}" and all of its content?`)) {
      this.albumService.deleteAlbum(this.album)
        .then(() => {
          this.router.navigateByUrl('/album');
        })
        .catch(() => {
          alert('Failed to delete album!');
        });
    }
  }

  onMouseEnterImage(image: AlbumItemDto) {
    this.hoveringImage = image;
  }

  onMouseLeaveImage(image: AlbumItemDto) {
    this.hoveringImage = null;
  }

  isMousingOverImage(image: AlbumItemDto) {
    return this.hoveringImage && this.hoveringImage.id === image.id;
  }

  onDeleteItemClick(image: AlbumItemDto) {
    if (confirm(`Would you like to delete the image "${image.itemName}"?`)) {
      this.albumItemService.deleteItem(image)
        .then(r => {
          if (!r) {
            alert(`Failed to delete image "${image.itemName}".`);
          }
          this.ngOnInit();
        });
    }
  }

  onImageClick(image: AlbumItemDto) {
    const modalRef = this.modalService.open(ImageModalComponent, {
      centered: true,
      size: 'lg',
      windowClass: 'image-modal'
    });
    const codeBehind = (modalRef.componentInstance as ImageModalComponent);
    codeBehind.init(image);
  }

}
