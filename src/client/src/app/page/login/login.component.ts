import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/utils/authentication.service';
import { SimpleLoginRequestBody } from 'src/app/entities/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  });
  showErrors = false;
  invalidLogin = false;

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.authService.isLoggedIn) {
      this.redirectToReturnUrl();
    }
  }

  redirectToReturnUrl() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.router.navigateByUrl(returnUrl || '/');
  }

  async onSubmit() {
    this.showErrors = true;
    this.invalidLogin = false;

    if (this.loginForm.valid) {
      const body = this.buildLoginBody();
      if (await this.authService.login(body)) {
        this.redirectToReturnUrl();
      } else {
        this.invalidLogin = true;
        this.password.setValue('');
      }
    }
  }

  buildLoginBody(): SimpleLoginRequestBody {
    return {
      username: this.username.value,
      password: this.password.value
    };
  }

  checkBasicFormValidity(control: AbstractControl, onlyIfTouched: boolean = true): boolean {
    return control.touched || !onlyIfTouched ? control.valid : true;
  }

  /* Form Controls */
  get username() { return this.loginForm.get('username'); }
  get usernameValid() { return this.checkBasicFormValidity(this.username); }

  get password() { return this.loginForm.get('password'); }
  get passwordValid() { return this.checkBasicFormValidity(this.password); }

  get isFormValid() {
    return this.usernameValid && this.passwordValid;
  }
  get isFormInvalid() { return !this.isFormValid; }

}
