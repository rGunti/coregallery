import { Component, OnInit } from '@angular/core';
import { AlbumDto, AlbumItemDto } from 'src/app/entities/album';
import { AlbumService } from 'src/app/data/album.service';
import { AuthenticationService } from 'src/app/utils/authentication.service';
import { PermissionKey } from 'src/app/utils/auth/permissions';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  albums: AlbumDto[];

  constructor(
    private albumService: AlbumService,
    private authService: AuthenticationService
  ) { }

  async ngOnInit() {
    this.albums = await this.albumService.getAlbums();
  }

  canCreateNewAlbum() {
    return this.authService.hasPermission(PermissionKey.ModifyAlbum);
  }

  onCreateNewAlbumClick(isPrivate: boolean) {
    const albumName = prompt('Please enter the name of the new album?');
    if (albumName) {
      this.albumService.createAlbum(albumName, isPrivate)
        .then((newAlbum) => {
          if (newAlbum) {
            this.ngOnInit();
          } else {
            alert(`Failed to create album "${albumName}".`);
          }
        })
        .catch((e) => {
          alert('Failed to create album.');
        });
    }
  }

}
