import { Injectable } from '@angular/core';
import { ApiService } from '../utils/api-service';
import { HttpClient } from '@angular/common/http';
import { RoleDto, PermissionDto } from '../entities/auth';

@Injectable({
  providedIn: 'root'
})
export class RolePermService extends ApiService {

  constructor(
    http: HttpClient
  ) {
    super(http);
  }

  async getRoles(): Promise<RoleDto[]> {
    try {
      return await this.get<RoleDto[]>(`/api/role`);
    } catch (e) {
      return null;
    }
  }

  async getRole(roleKey: string): Promise<RoleDto> {
    try {
      return await this.get<RoleDto>(`/api/role/${roleKey}`);
    } catch (e) {
      return null;
    }
  }

  async getPermissions(): Promise<PermissionDto[]> {
    try {
      return await this.get<PermissionDto[]>(`/api/permission`);
    } catch (e) {
      return null;
    }
  }
}
