import { Injectable } from '@angular/core';
import { ApiService } from '../utils/api-service';
import { HttpClient } from '@angular/common/http';
import { UserDto, RoleDto } from '../entities/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiService {

  constructor(
    http: HttpClient
  ) {
    super(http);
  }

  async getAllUsers(): Promise<UserDto[]> {
    try {
      return await this.get<UserDto[]>('/api/user');
    } catch (err) {
      return [];
    }
  }

  async deleteUser(user: UserDto): Promise<boolean> {
    try {
      await this.delete(`/api/user/${user.id}`);
      return true;
    } catch (e) {
      return false;
    }
  }

  async reactivateUser(user: UserDto): Promise<UserDto> {
    try {
      return await this.post<any, UserDto>(`/api/user/${user.id}/reactivate`, null);
    } catch (e) {
      return null;
    }
  }

  async resetPasswordFor(user: UserDto): Promise<UserDto> {
    try {
      return await this.post<any, UserDto>(`/api/user/${user.id}/reset`, null);
    } catch (e) {
      return null;
    }
  }

  async getUser(username: string): Promise<UserDto> {
    try  {
      return await this.get<UserDto>(`/api/user/${username}`);
    } catch (e) {
      return null;
    }
  }

  async getRoles(): Promise<RoleDto[]> {
    try {
      return await this.get<RoleDto[]>(`/api/role`);
    } catch (e) {
      return null;
    }
  }

  async createUser(user: UserDto): Promise<UserDto> {
    try {
      return await this.post<UserDto, UserDto>(`/api/user`, user);
    } catch (e) {
      return null;
    }
  }

  async updateUser(user: UserDto): Promise<boolean> {
    try {
      await this.put<UserDto, any>(`/api/user/${user.id}`, user);
      return true;
    } catch (e) {
      return false;
    }
  }
}
