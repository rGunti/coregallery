import { Injectable } from '@angular/core';
import { ApiService } from '../utils/api-service';
import { HttpClient } from '@angular/common/http';
import { AlbumDto, AlbumItemDto } from '../entities/album';
import { IPagedResult } from '../entities/base';

@Injectable({
  providedIn: 'root'
})
export class AlbumService extends ApiService {

  constructor(
    http: HttpClient
  ) {
    super(http);
  }

  async getAlbums(): Promise<AlbumDto[]> {
    try {
      const albums = await this.get<AlbumDto[]>('/api/album');
      return albums;
    } catch (err) {
      console.error(`Failed to fetch album list`, err);
      return [];
    }
  }

  async getAlbum(albumId: number): Promise<AlbumDto> {
    try {
      const album = await this.get<AlbumDto>(`/api/album/${albumId}`);
      return album;
    } catch (err) {
      console.error(`Failed to fetch album ${albumId}`, err);
      return null;
    }
  }

  async getAlbumItems(albumId: number, page: number = 0): Promise<IPagedResult<AlbumItemDto>> {
    try {
      return await this.get<IPagedResult<AlbumItemDto>>(`/api/album/${albumId}/item?page=${page}`);
    } catch (err) {
      console.error(`Failed to fetch album items in album ${albumId}`, err);
      return null;
    }
  }

  getAlbumThumbnailUrl(albumItem: AlbumItemDto): string {
    return this.getUrl(`/api/album/${albumItem.albumID}/item/${albumItem.id}/image/thumb`);
  }

  async getAlbumItemThumbnail(albumItem: AlbumItemDto): Promise<string> {
    try {
      const blob = await this.getBlob(`/api/album/${albumItem.albumID}/item/${albumItem.id}/image/thumb`);
      return URL.createObjectURL(blob);
    } catch (err) {
      console.error(`Failed to fetch album item thumbnail ${albumItem.albumID}/${albumItem.id}`, err);
      return null;
    }
  }

  async createAlbum(albumTitle: string, isPrivate: boolean = false): Promise<AlbumDto> {
    try {
      const album: AlbumDto = {
        id: null,
        createdAt: null,
        updatedAt: null,
        name: albumTitle,
        userID: null,
        isPrivateAlbum: isPrivate
      };
      return await this.post<AlbumDto, AlbumDto>(`/api/album`, album);
    } catch (err) {
      console.error(`Failed to create album with name ${albumTitle}`, err);
      return null;
    }
  }

  async deleteAlbum(album: AlbumDto): Promise<any> {
    try {
      await this.delete(`/api/album/${album.id}`);
    } catch (err) {
      console.error('Failed to delete album');
    }
  }
}
