import { Injectable } from '@angular/core';
import { ApiService } from '../utils/api-service';
import { HttpClient } from '@angular/common/http';
import { AlbumDto } from '../entities/album';
import { AlbumItemDto } from '../entities/album';
import { INeighborhood } from '../entities/base';

@Injectable({
  providedIn: 'root'
})
export class AlbumItemService extends ApiService {

  constructor(
    http: HttpClient
  ) {
    super(http);
  }

  uploadFile(album: AlbumDto, file: File): Promise<any> {
    const formData: FormData = new FormData();
    formData.append('imageFile', file);

    return new Promise<any>((resolve, reject) => {
      this.http.post(
        this.getUrl(`/api/album/${album.id}/item`),
        formData
      ).subscribe(() => {
        resolve(true);
      }, (err) => {
        reject(err);
      });
    });
  }

  async deleteItem(albumItem: AlbumItemDto): Promise<boolean> {
    try {
      await this.delete<any>(`/api/album/${albumItem.albumID}/item/${albumItem.id}`);
      return true;
    } catch {
      return false;
    }
  }

  async getNeighborhood(albumItem: AlbumItemDto): Promise<INeighborhood<AlbumItemDto>> {
    try {
      return await this.get<any>(`/api/album/${albumItem.albumID}/item/${albumItem.id}/neighborhood`);
    } catch (e) {
      return null;
    }
  }

  async getFullResImageUrl(albumItem: AlbumItemDto): Promise<string> {
    try {
      const blob = await this.getBlob(`/api/album/${albumItem.albumID}/item/${albumItem.id}/image`);
      return URL.createObjectURL(blob);
    } catch (e) {
      return null;
    }
  }
}
